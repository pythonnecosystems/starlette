Starlette는 널리 사용되는 [OpenAPI specification](https://github.com/OAI/OpenAPI-Specification)같은 API 스키마 생성을 지원한다. (이전에는 "Swagger"로 알려졌다.)

스키마 생성은 `app.routes`를 통해 어플리케이션의 경로를 검사하고 엔드포인트의 docstrings 또는 기타 속성을 사용하여 완전한 API 스키마를 결정하는 방식으로 작동한다.

Starlette는 특정 스키마 생성이나 유효성 검사 도구에 종속되어 있지 않으며, docstrings을 기반으로 OpenAPI 스키마를 생성하는 간단한 구현이 포함되어 있다.

```python
from starlette.applications import Starlette
from starlette.routing import Route
from starlette.schemas import SchemaGenerator


schemas = SchemaGenerator(
    {"openapi": "3.0.0", "info": {"title": "Example API", "version": "1.0"}}
)

def list_users(request):
    """
    responses:
      200:
        description: A list of users.
        examples:
          [{"username": "tom"}, {"username": "lucy"}]
    """
    raise NotImplementedError()


def create_user(request):
    """
    responses:
      200:
        description: A user.
        examples:
          {"username": "tom"}
    """
    raise NotImplementedError()


def openapi_schema(request):
    return schemas.OpenAPIResponse(request=request)


routes = [
    Route("/users", endpoint=list_users, methods=["GET"]),
    Route("/users", endpoint=create_user, methods=["POST"]),
    Route("/schema", endpoint=openapi_schema, include_in_schema=False)
]

app = Starlette(routes=routes)
```

이제 "/schema" 엔드포인트에서 OpenAPI 스키마에 액세스할 수 있다.

`.get_schema(routes)`를 사용하여 API 스키마를 직접 생성할 수 있다.

```python
schema = schemas.get_schema(routes=app.routes)
assert schema == {
    "openapi": "3.0.0",
    "info": {"title": "Example API", "version": "1.0"},
    "paths": {
        "/users": {
            "get": {
                "responses": {
                    200: {
                        "description": "A list of users.",
                        "examples": [{"username": "tom"}, {"username": "lucy"}],
                    }
                }
            },
            "post": {
                "responses": {
                    200: {"description": "A user.", "examples": {"username": "tom"}}
                }
            },
        },
    },
}
```

API 문서 생성 등의 툴을 사용할 수 있도록 API 스키마를 인쇄할 수 있는 기능도 필요할 수 있다.

```python
if __name__ == '__main__':
    assert sys.argv[-1] in ("run", "schema"), "Usage: example.py [run|schema]"

    if sys.argv[-1] == "run":
        uvicorn.run("example:app", host='0.0.0.0', port=8000)
    elif sys.argv[-1] == "schema":
        schema = schemas.get_schema(routes=app.routes)
        print(yaml.dump(schema, default_flow_style=False))
```

## 서드 파티 패키지

### [starlette-apispec](https://github.com/Woile/starlette-apispec)
일부 객체 직렬화 라이브러리를 지원하는 Starlette에 대한 간편한 APISpec 통합.
