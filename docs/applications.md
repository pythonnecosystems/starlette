Starlette에는 다른 모든 기능을 잘 묶어주는 어플리케이션 클래스 `Starlette`가 포함되어 있다.

```python
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.routing import Route, Mount, WebSocketRoute
from starlette.staticfiles import StaticFiles


def homepage(request):
    return PlainTextResponse('Hello, world!')

def user_me(request):
    username = "John Doe"
    return PlainTextResponse('Hello, %s!' % username)

def user(request):
    username = request.path_params['username']
    return PlainTextResponse('Hello, %s!' % username)

async def websocket_endpoint(websocket):
    await websocket.accept()
    await websocket.send_text('Hello, websocket!')
    await websocket.close()

def startup():
    print('Ready to go')


routes = [
    Route('/', homepage),
    Route('/user/me', user_me),
    Route('/user/{username}', user),
    WebSocketRoute('/ws', websocket_endpoint),
    Mount('/static', StaticFiles(directory="static")),
]

app = Starlette(debug=True, routes=routes, on_startup=[startup])
```

## 어플리케이션 인스턴스화
*class* `starlette.applications`.**Starlette**(*debug=False*, *routes=None*, *middleware=None*, *exception_handlers=None*, *on_startup=None*, *on_shutdown=None*, *lifespan=None*)

어플리케이션 인스턴스를 생성한다.

**Parameters**:

- **debug** - 오류 발생 시 디버그 트레이스백을 반환할지 여부를 나타내는 부울리언이다.
- **routes** - 들어오는 HTTP와 WebSocket 요청을 처리할 경로 목록이다.
- **middleware** - 모든 요청에 대해 실행할 미들웨어 목록이다. starlette 어플리케이션은 항상 자동으로 두 개의 미들웨어 클래스를 포함한다. `ServerErrorMiddleware`는 가장 바깥쪽 미들웨어로 추가되어 전체 스택의 어느 곳에서든 발생하는 잡히지 않은 오류를 처리한다. `ExceptionMiddleware`는 라우팅 또는 엔드포인트에서 발생하는 예외 사례를 처리하기 위해 가장 안쪽 미들웨어로 추가된다.
- **exception_handlers** - 정수 상태 코드 또는 예외 클래스 타입을 예외를 처리하는 콜러블에 매핑한다. 예외 처리기 콜러블은 `handler(request, exc) -> response 패턴이어야 하며 표준 함수 또는 비동기 함수일 수 있다.
- **on_startup** - 어플리케이션을 시작할 때 실행할 콜러블 목록이다. 시작 핸들러 콜러블은 인수를 받지 않으며, 표준 함수 또는 비동기 함수일 수 있다.
- **on_shutdown** - 어플리케이션을 종료할 때 실행할 콜러블 목록이다. 종료 핸들러 콜러블은 인수를 받지 않으며, 표준 함수 또는 비동기 함수일 수 있다.
- **lifespan** - 시작과 종료 작업을 수행하는 데 사용할 수 있는 수명 컨텍스트 함수이다. 이것은 `on_startup`과 `on_shutdown` 핸들러를 대체하는 새로운 스타일이다. 둘 중 하나만 사용한다.

## 앱 인스턴스에 상태 저장
일반 `app.state` 속성을 사용하여 어플리케이션 인스턴스에 임의의 추가 상태를 저장할 수 있다.

예를 들어,

```python
app.state.ADMIN_EMAIL = 'admin@example.org'
```

## 앱 인스턴스 접근
`request`를 사용할 수 있는 경우 (예: 엔드포인트와 미들웨어) `request.app`에서 앱을 사용할 수 있다.
