Starlette는 인증과 권한 처리를 위한 간단하지만 강력한 인터페이스를 제공gks다. 적절한 인증 백엔드가 있는 `AuthenticationMiddleware`를 설치하면 `request.user`와 `request.auth` 인터페이스를 엔드포인트에서 사용할 수 있다.

```python
from starlette.applications import Starlette
from starlette.authentication import (
    AuthCredentials, AuthenticationBackend, AuthenticationError, SimpleUser
)
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.responses import PlainTextResponse
from starlette.routing import Route
import base64
import binascii


class BasicAuthBackend(AuthenticationBackend):
    async def authenticate(self, conn):
        if "Authorization" not in conn.headers:
            return

        auth = conn.headers["Authorization"]
        try:
            scheme, credentials = auth.split()
            if scheme.lower() != 'basic':
                return
            decoded = base64.b64decode(credentials).decode("ascii")
        except (ValueError, UnicodeDecodeError, binascii.Error) as exc:
            raise AuthenticationError('Invalid basic auth credentials')

        username, _, password = decoded.partition(":")
        # TODO: You'd want to verify the username and password here.
        return AuthCredentials(["authenticated"]), SimpleUser(username)


async def homepage(request):
    if request.user.is_authenticated:
        return PlainTextResponse('Hello, ' + request.user.display_name)
    return PlainTextResponse('Hello, you')

routes = [
    Route("/", endpoint=homepage)
]

middleware = [
    Middleware(AuthenticationMiddleware, backend=BasicAuthBackend())
]

app = Starlette(routes=routes, middleware=middleware)
```

## 사용자
`AuthenticationMiddleware`가 설치되면 `request.user` 인터페이스를 엔드포인트나 다른 미들웨어에서 사용할 수 있다.

이 인터페이스는 사용자 모델에 포함된 다른 정보뿐만 아니라 두 가지 프로퍼티를 제공하는 `BaseUser`의 서브클래스여야 한다.

- `.is_authenticated`
- `.display_name`

Starlette는 두 가지 내장 사용자 구현을 제공한다. 이는 `UnauthenticatedUser()`와 `SimpleUser(username)`이다.

## 인증 자격 증명(AuthCredentials)
인증 자격 증명을 사용자와 별개의 개념으로 취급하는 것이 중요하다. 인증 체계는 사용자 ID와 독립적으로 특정 권한을 제한하거나 부여할 수 있어야 한다.

`AuthCredentials` 클래스는 `request.auth`가 노출하는 기본 인터페이스를 제공한다.

- `.scopes`

## 권한
권한은 엔드포인트 데코레이터로 구현되어 들어오는 요청에 필요한 인증 범위가 포함되어야 한다.

```python
from starlette.authentication import requires


@requires('authenticated')
async def dashboard(request):
    ...
```

하나 또는 다수의 필수 범위를 포함할 수 있다.

```python
from starlette.authentication import requires


@requires(['authenticated', 'admin'])
async def dashboard(request):
    ...
```

기본적으로 권한이 부여되지 않은 경우 403 응답이 반환된다. 인증되지 않은 사용자로부터 URL 레이아웃에 대한 정보를 숨기는 등 경우에 따라 이를 커스터마이즈하고 싶을 수 있다.

```python
from starlette.authentication import requires


@requires(['authenticated', 'admin'], status_code=404)
async def dashboard(request):
    ...
```

> **Note**
>
> `status_code` 매개변수는 WebSocket에서 지원되지 않는다. 이러한 경우 항상 403(Forbidden) 상태 코드가 사용된다.

또는 인증되지 않은 사용자를 다른 페이지로 리디렉션할 수도 있다.

```python
from starlette.authentication import requires


async def homepage(request):
    ...


@requires('authenticated', redirect='homepage')
async def dashboard(request):
    ...
```

사용자를 리디렉션할 때 리디렉션하는 페이지에는 사용자가 원래 `next` 쿼리 매개변수에서 요청했던 URL을 포함한다.

```python
from starlette.authentication import requires
from starlette.responses import RedirectResponse


@requires('authenticated', redirect='login')
async def admin(request):
    ...


async def login(request):
    if request.method == "POST":
        # Now that the user is authenticated,
        # we can send them to their original request destination
        if request.user.is_authenticated:
            next_url = request.query_params.get("next")
            if next_url:
                return RedirectResponse(next_url)
            return RedirectResponse("/")
```

클래스 기반 엔드포인트의 경우 클래스의 메서드를 데코레이터로 감싸야 한다.

```python
from starlette.authentication import requires
from starlette.endpoints import HTTPEndpoint


class Dashboard(HTTPEndpoint):
    @requires("authenticated")
    async def get(self, request):
        ...
```

## 커스텀 인증 오류 응답
인증 백엔드에서 `AuthenticationError`가 발생할 때 전송되는 오류 응답을 커스터마이즈할 수 있다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse


def on_auth_error(request: Request, exc: Exception):
    return JSONResponse({"error": str(exc)}, status_code=401)

app = Starlette(
    middleware=[
        Middleware(AuthenticationMiddleware, backend=BasicAuthBackend(), on_error=on_auth_error),
    ],
)
```
