Starlette는 [12요소 패턴](https://12factor.net/config)에 따라 구성과 코드를 엄격하게 분리할 것을 권장한다.

구성은 환경 변수 또는 소스 제어에 커밋되지 않은 '.env' 파일에 저장해야 한다.

**app.py**

```python
import databases

from starlette.applications import Starlette
from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret

# Config will be read from environment variables and/or ".env" files.
config = Config(".env")

DEBUG = config('DEBUG', cast=bool, default=False)
DATABASE_URL = config('DATABASE_URL', cast=databases.DatabaseURL)
SECRET_KEY = config('SECRET_KEY', cast=Secret)
ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=CommaSeparatedStrings)

app = Starlette(debug=DEBUG)
...
```

**.env**

```python
# Don't commit this to source control.
# Eg. Include ".env" in your `.gitignore` file.
DEBUG=True
DATABASE_URL=postgresql://localhost/myproject
SECRET_KEY=43n080musdfjt54t-09sdgr
ALLOWED_HOSTS=127.0.0.1, localhost
```

## 구성 우선 순위
구성 값을 읽는 순서는 다음과 같습니다:

- 환경 변수에서.
- ".env" 파일에서.
- `config`에 지정된 기본값.

이 중 일치하는 것이 없으면 `config(...)`는 오류를 발생시킨다.

## Secrets
민감한 키의 경우 `Secret` 클래스가 유용한데, 이는 해당 값이 트레이스백이나 기타 코드 내성 검사로 유출될 수 있는 경우를 최소화하는 데 도움이 되기 때문이다.

`Secret` 인스턴스의 값을 가져오려면 명시적으로 문자열로 캐스팅해야 한다. 이 작업은 값이 사용되는 지점에서만 수행해야 한다.

```python
>>> from myproject import settings
>>> settings.SECRET_KEY
Secret('**********')
>>> str(settings.SECRET_KEY)
'98n349$%8b8-7yjn0n8y93T$23r'
```

마찬가지로 `URL` 클래스는 표현에서 비밀번호 구성 요소를 숨긴다.

```python
>>> from myproject import settings
>>> settings.DATABASE_URL
DatabaseURL('postgresql://admin:**********@192.168.0.8/my-application')
>>> str(settings.DATABASE_URL)
```

## CommaSeperateStrings
단일 구성 키 안에 여러 개를 보유하려면 `CommaSeparatedStrings` 타입이 유용하다.

```python
>>> from myproject import settings
>>> print(settings.ALLOWED_HOSTS)
CommaSeparatedStrings(['127.0.0.1', 'localhost'])
>>> print(list(settings.ALLOWED_HOSTS))
['127.0.0.1', 'localhost']
>>> print(len(settings.ALLOWED_HOSTS))
2
>>> print(settings.ALLOWED_HOSTS[0])
'127.0.0.1'
```

## 환경 읽기와 수정
프로그래밍 방식으로 환경 변수를 읽거나 수정해야 하는 경우도 있다. 이는 환경의 특정 키를 재정의해야 하는 테스트에 특히 유용하다.

`os.environ`에서 읽거나 쓰는 대신 Starlette의 `environ` 인스턴스를 사용해야 한다. 이 인스턴스는 표준 `os.environ`에 대한 매핑으로, 환경 변수가 구성에서 이미 읽은 시점 *이후*에 설정된 경우 오류를 발생시켜 사용자를 추가로 보호한다.

`pytest`를 사용하는 경우 `tests/conftest.py`에서 초기 환경을 설정할 수 있다.

**test/conftest.py**

```python
from starlette.config import environ

environ['TESTING'] = 'TRUE'
```

## 접두사가 붙은 환경 변수 읽기
`env_prefix` 인수를 설정하여 환경 변수의 네임스페이스를 지정할 수 있다.

**myproject/settings.py**

```python
import os
from starlette.config import Config

os.environ['APP_DEBUG'] = 'yes'
os.environ['ENVIRONMENT'] = 'dev'

config = Config(env_prefix='APP_')

DEBUG = config('DEBUG') # lookups APP_DEBUG, returns "yes"
ENVIRONMENT = config('ENVIRONMENT') # lookups APP_ENVIRONMENT, raises KeyError as variable is not defined
```

## 전체 예
대규모 어플리케이션의 구조화는 복잡할 수 있다. 구성과 코드의 적절한 분리, 테스트 중 데이터베이스 격리, 테스트 데이터베이스와 프로덕션 데이터베이스의 분리 등이 필요하다.

여기서는 어플리케이션 구조화를 시작하는 방법을 보여주는 전체 예를 살펴보겠다.

먼저 설정, 데이터베이스 테이블 정의, 어플리케이션 로직을 분리해 보겠다.

**myproject/settings.py**

```python
import databases
from starlette.config import Config
from starlette.datastructures import Secret

config = Config(".env")

DEBUG = config('DEBUG', cast=bool, default=False)
TESTING = config('TESTING', cast=bool, default=False)
SECRET_KEY = config('SECRET_KEY', cast=Secret)

DATABASE_URL = config('DATABASE_URL', cast=databases.DatabaseURL)
if TESTING:
    DATABASE_URL = DATABASE_URL.replace(database='test_' + DATABASE_URL.database)
```

**myproject/tables.py**

```python
import sqlalchemy

# Database table definitions.
metadata = sqlalchemy.MetaData()

organisations = sqlalchemy.Table(
    ...
)
```

**myproject/app.py**

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.routing import Route
from myproject import settings


async def homepage(request):
    ...

routes = [
    Route("/", endpoint=homepage)
]

middleware = [
    Middleware(
        SessionMiddleware,
        secret_key=settings.SECRET_KEY,
    )
]

app = Starlette(debug=settings.DEBUG, routes=routes, middleware=middleware)
```

이제 테스트 구성을 다루겠다. 테스트 스위트가 실행될 때마다 새 테스트 데이터베이스를 생성하고 테스트가 완료되면 삭제하고 싶다. 또한 다음을 보장하고 싶다.

**tests/conftest.py**

```python
from starlette.config import environ
from starlette.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, database_exists, drop_database

# This line would raise an error if we use it after 'settings' has been imported.
environ['TESTING'] = 'TRUE'

from myproject import settings
from myproject.app import app
from myproject.tables import metadata


@pytest.fixture(autouse=True, scope="session")
def setup_test_database():
    """
    Create a clean test database every time the tests are run.
    """
    url = str(settings.DATABASE_URL)
    engine = create_engine(url)
    assert not database_exists(url), 'Test database already exists. Aborting tests.'
    create_database(url)             # Create the test database.
    metadata.create_all(engine)      # Create the tables.
    yield                            # Run the tests.
    drop_database(url)               # Drop the test database.


@pytest.fixture()
def client():
    """
    Make a 'client' fixture available to test cases.
    """
    # Our fixture is created within a context manager. This ensures that
    # application lifespan runs for every test case.
    with TestClient(app) as test_client:
        yield test_client
```
