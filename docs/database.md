Starlette은 특정 데이터베이스 구현에 엄격하게 얽매이지 않는다.

[GINO](https://python-gino.org/)와 같은 비동기 ORM과 함께 사용하거나 일반 비동기 엔드포인트를 사용하여 [SQLAlchemy](https://www.sqlalchemy.org/)와 통합할 수 있다.

여기에서는 다양한 데이터베이스 드라이버에 대해 SQLAlchemy 핵심 지원을 제공하는 [`databases` 패키지](https://github.com/encode/databases)에 통합하는 방법을 보이겠다.

여기에는 테이블 정의, `database.Database` 인스턴스 구성과 데이터베이스와 상호 작용하는 몇몇 엔드포인트가 포함된 완벽한 예를 포함하고 있다.

**.env**

```python
DATABASE_URL=sqlite:///test.db
```

**app.py**

```python
import contextlib

import databases
import sqlalchemy
from starlette.applications import Starlette
from starlette.config import Config
from starlette.responses import JSONResponse
from starlette.routing import Route


# Configuration from environment variables or '.env' file.
config = Config('.env')
DATABASE_URL = config('DATABASE_URL')


# Database table definitions.
metadata = sqlalchemy.MetaData()

notes = sqlalchemy.Table(
    "notes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("text", sqlalchemy.String),
    sqlalchemy.Column("completed", sqlalchemy.Boolean),
)

database = databases.Database(DATABASE_URL)

@contextlib.asynccontextmanager
async def lifespan(app):
    await database.connect()
    yield
    await database.disconnect()

# Main application code.
async def list_notes(request):
    query = notes.select()
    results = await database.fetch_all(query)
    content = [
        {
            "text": result["text"],
            "completed": result["completed"]
        }
        for result in results
    ]
    return JSONResponse(content)

async def add_note(request):
    data = await request.json()
    query = notes.insert().values(
       text=data["text"],
       completed=data["completed"]
    )
    await database.execute(query)
    return JSONResponse({
        "text": data["text"],
        "completed": data["completed"]
    })

routes = [
    Route("/notes", endpoint=list_notes, methods=["GET"]),
    Route("/notes", endpoint=add_note, methods=["POST"]),
]

app = Starlette(
    routes=routes,
    lifespan=lifespan,
)
```

마지막으로 데이터베이스 테이블을 생성해야 한다. [마이그레이션](#migrations)에서 간략하게 살펴본 Alembic을 사용하는 것이 좋다.

## 쿼리
쿼리는 S[QLAlchemy Core queries](https://docs.sqlalchemy.org/en/latest/core/)로 수행할 수 있다.

지원되는 메서드는 다음과 같다.

- `rows = await database.fetch_all(query)`
- `row = await database.fetch_one(query)`
- `async for row in database.iterate(query)`
- `await database.execute(query)`
- `await database.execute_many(query)`

## 트랜잭션
데이터베이스 트랜잭션을 데코레이터, 컨텍스트 관리자 또는 로우레벨 API로 수행할 수 있다.

엔드포인트에서 데코레이터 사용...

```python
@database.transaction()
async def populate_note(request):
    # This database insert occurs within a transaction.
    # It will be rolled back by the `RuntimeError`.
    query = notes.insert().values(text="you won't see me", completed=True)
    await database.execute(query)
    raise RuntimeError()
```

컨텍스트 관리자 사용...

```python
async def populate_note(request):
    async with database.transaction():
        # This database insert occurs within a transaction.
        # It will be rolled back by the `RuntimeError`.
        query = notes.insert().values(text="you won't see me", completed=True)
        await request.database.execute(query)
        raise RuntimeError()
```

로우레벨 API ...

```python
async def populate_note(request):
    transaction = await database.transaction()
    try:
        # This database insert occurs within a transaction.
        # It will be rolled back by the `RuntimeError`.
        query = notes.insert().values(text="you won't see me", completed=True)
        await database.execute(query)
        raise RuntimeError()
    except:
        await transaction.rollback()
        raise
    else:
        await transaction.commit()
```

## 테스트 격리
데이터베이스를 사용하는 서비스에 대해 테스트를 실행할 때 확인해야 할 몇 가지 사항이 있다. 요구 사항은 다음과 같다.

- 테스트에 별도의 데이터베이스를 사용한다.
- 테스트를 실행할 때마다 새로운 테스트 데이터베이스를 생성한다.
- 각 테스트 케이스 간에 데이터베이스 상태가 격리되어 있는지 확인한다.

이러한 요구 사항을 충족하기 위해 어플리케이션과 테스트를 구조화해야 하는 방법은 다음과 같다.

```python
from starlette.applications import Starlette
from starlette.config import Config
import databases

config = Config(".env")

TESTING = config('TESTING', cast=bool, default=False)
DATABASE_URL = config('DATABASE_URL', cast=databases.DatabaseURL)
TEST_DATABASE_URL = DATABASE_URL.replace(database='test_' + DATABASE_URL.database)

# Use 'force_rollback' during testing, to ensure we do not persist database changes
# between each test case.
if TESTING:
    database = databases.Database(TEST_DATABASE_URL, force_rollback=True)
else:
    database = databases.Database(DATABASE_URL)
```

테스트 실행 중에 `TESTING`을 설정하고 테스트 데이터베이스를 설정해야 한다. `py.test`를 사용한다고 가정할 때, `conftest.py`의 모습은 다음과 같다.

```python
import pytest
from starlette.config import environ
from starlette.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database, drop_database

# This sets `os.environ`, but provides some additional protection.
# If we placed it below the application import, it would raise an error
# informing us that 'TESTING' had already been read from the environment.
environ['TESTING'] = 'True'

import app


@pytest.fixture(scope="session", autouse=True)
def create_test_database():
  """
  Create a clean database on every test case.
  For safety, we should abort if a database already exists.

  We use the `sqlalchemy_utils` package here for a few helpers in consistently
  creating and dropping the database.
  """
  url = str(app.TEST_DATABASE_URL)
  engine = create_engine(url)
  assert not database_exists(url), 'Test database already exists. Aborting tests.'
  create_database(url)             # Create the test database.
  metadata.create_all(engine)      # Create the tables.
  yield                            # Run the tests.
  drop_database(url)               # Drop the test database.


@pytest.fixture()
def client():
    """
    When using the 'client' fixture in test cases, we'll get full database
    rollbacks between test cases:

    def test_homepage(client):
        url = app.url_path_for('homepage')
        response = client.get(url)
        assert response.status_code == 200
    """
    with TestClient(app) as client:
        yield client
```

## <a name="migration"></a>마이그레션
데이터베이스의 점진적인 변경 사항을 관리하려면 거의 확실하게 데이터베이스 마이그레이션을 사용해야 한다. 이를 위해 SQLAlchemy의 저자가 작성한 Alembic을 강력히 추천한다.

```bash
$ pip install alembic
$ alembic init migrations
```

이제 Alembic이 구성된 DATABASE_URL을 참조하고 테이블 메타데이터를 사용하도록 설정해야 한다.

`alembic.ini`에서 다음 줄을 제거한다.

```python
sqlalchemy.url = driver://user:pass@localhost/dbname
```

`migrations/env.py`에서 `'sqlalchemy.url'` 구성 키와 `target_metadata` 변수를 설정해야 한다. 다음과 같은 설정이 필요하다.

```python
# The Alembic Config object.
config = context.config

# Configure Alembic to use our DATABASE_URL and our table definitions...
import app
config.set_main_option('sqlalchemy.url', str(app.DATABASE_URL))
target_metadata = app.metadata

...

```

다음 위의 노트 예시를 사용해 초기 수정본을 만든다.

```
alembic revision -m "Create notes table"
```

그리고 (`migrations/version` 내)새 파일에 필요한  명령을 채운다.

```python
def upgrade():
    op.create_table(
      'notes',
      sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
      sqlalchemy.Column("text", sqlalchemy.String),
      sqlalchemy.Column("completed", sqlalchemy.Boolean),
    )

def downgrade():
    op.drop_table('notes')
```

그리고 첫 마이그레이션을 실행하자. 이제 노트 앱을 실행할 수 있다!

```
alembic upgrade head
```

### 테스팅 동안 마이그레션 수행
테스트 스위트가 테스트 데이터베이스를 만들 때마다 데이터베이스 마이그레이션을 실행하도록 하는 것이 좋다. 이렇게 하면 마이그레이션 스크립트에서 문제를 발견하는 데 도움이 되며, 라이브 데이터베이스와 일관된 상태의 데이터베이스에 대해 테스트가 실행되는지 확인할 수 있다.

`create_test_database` fixture를 약간 조정할 수 있다.

```python
from alembic import command
from alembic.config import Config
import app

...

@pytest.fixture(scope="session", autouse=True)
def create_test_database():
    url = str(app.DATABASE_URL)
    engine = create_engine(url)
    assert not database_exists(url), 'Test database already exists. Aborting tests.'
    create_database(url)             # Create the test database.
    config = Config("alembic.ini")   # Run the migrations.
    command.upgrade(config, "head")
    yield                            # Run the tests.
    drop_database(url)               # Drop the test database.
```
