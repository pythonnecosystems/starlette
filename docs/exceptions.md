Starlette을 사용하면 커스텀 예외 처리기를 설치하여 오류 또는 처리된 예외가 발생할 때 응답을 반환하는 방법을 처리할 수 있다.

```python
from starlette.applications import Starlette
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import HTMLResponse


HTML_404_PAGE = ...
HTML_500_PAGE = ...


async def not_found(request: Request, exc: HTTPException):
    return HTMLResponse(content=HTML_404_PAGE, status_code=exc.status_code)

async def server_error(request: Request, exc: HTTPException):
    return HTMLResponse(content=HTML_500_PAGE, status_code=exc.status_code)


exception_handlers = {
    404: not_found,
    500: server_error
}

app = Starlette(routes=routes, exception_handlers=exception_handlers)
```

`debug`가 활성화되어 있고 오류가 발생하면 설치된 500 핸들러를 사용하는 대신 Starlette에서 트레이스백 응답으로 응답한다.

```python
app = Starlette(debug=True, routes=routes, exception_handlers=exception_handlers)
```

특정 상태 코드에 대한 핸들러를 등록하는 것뿐만 아니라 예외 클래스에 대한 핸들러도 등록할 수 있다.

특히 기본 제공 `HTTPException` 클래스의 처리 방식을 재정의하고 싶을 수 있다. 예를 들어 JSON 스타일 응답을 사용할 수 있다.

```python
async def http_exception(request: Request, exc: HTTPException):
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)

exception_handlers = {
    HTTPException: http_exception
}
```

`HTTPException`에는 `headers`인수도 있다. 이를 통해 헤더를 응답 클래스로 전파할 수 있다.

```python
async def http_exception(request: Request, exc: HTTPException):
    return JSONResponse(
        {"detail": exc.detail},
        status_code=exc.status_code,
        headers=exc.headers
    )
```

`WebSocketException`이 처리되는 방식을 재정의할 수도 있다.

```python
async def websocket_exception(websocket: WebSocket, exc: WebSocketException):
    await websocket.close(code=1008)

exception_handlers = {
    WebSocketException: websocket_exception
}
```

## 오류와 처리된 예외
처리된 예외와 오류를 구분하는 것은 중요하다.

처리된 예외는 오류 사례를 나타내지 않는다. 처리된 예외는 적절한 HTTP 응답으로 강제로 변환되어 표준 미들웨어 스택을 통해 전송된다. 기본적으로 처리된 예외를 관리하는 데는 `HTTPException` 클래스를 사용한다.

오류는 애플리케이션 내에서 발생하는 또 다른 예외이다. 이러한 경우는 전체 미들웨어 스택에서 예외로 버블 처리되어야 한다. 모든 오류 로깅 미들웨어는 서버에 이르는 모든 방법의 예외를 다시 발생시켜야 한다.

실제로 사용되는 오류 처리 방식은 `exception_handler[500]` 또는 `exception_handler[Exception]`이다. `500` 키와 `Exception` 키를 모두 사용할 수 있다. 아래를 참조하시오.

```python
async def handle_error(request: Request, exc: HTTPException):
    # Perform some logic
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)

exception_handlers = {
    Exception: handle_error  # or "500: handle_error"
}
```

[BackgroundTask](./background-tasks.md)가 예외를 발생시키면 `handle_error` 함수가 처리하지만, 그 시점에서는 이미 응답이 전송되었다는 점에 유의해야 한다. 즉, `handle_error`가 생성한 응답은 버려진다. 응답이 전송되기 전에 오류가 발생하면 응답 객체(위의 예에서는 반환된 `JSONResponse`)를 사용한다.

이 동작을 정확히 처리하기 위해 Starlette 어플리케이션의 미들웨어 스택은 다음과 같이 구성된다.

- `ServerErrorMiddleware` - 서버 오류 발생 시 500 응답을 반환한다.
- 설치된 미들웨어
- `ExceptionMiddleware` - 처리된 예외를 처리하고 응답을 반환한다.
- 라우터
- 엔드포인트

## HTTPException
`HTTPException` 클래스는 처리되는 모든 예외에 사용할 수 있는 베이스 클래스를 제공한다. `ExceptionMiddleware` 구현은 기본적으로 모든 `HTTPException`에 대해 일반 텍스트 HTTP 응답을 반환한다.

- `HTTPException(status_code, detail=None, headers=None)`

라우팅 또는 엔드포인트 내부에서만 `HTTPException`을 발생시켜야 한다. 미들웨어 클래스는 대신 적절한 응답을 직접 반환해야 한다.

## WebSocketException
`WebSocketException` 클래스를 사용하여 WebSocket 엔드포인트 내부에서 오류를 발생시킬 수 있다.

- `WebSocketException(code=1008, reason=None)`

[사양](https://tools.ietf.org/html/rfc6455#section-7.4.1)에 정의된 대로 유효한 코드를 설정할 수 있다.
