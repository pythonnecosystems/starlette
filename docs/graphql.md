Starlette의 GraphQL 지원은 버전 0.15.0에서 더 이상 사용되지 않으며, 버전 0.17.0에서 제거되었다.

더 이상 Starlette에 GraphQL 지원이 내장되어 있지 않지만, 타사 라이브러리를 통해 Starlette에서 GraphQL을 계속 사용할 수 있다. 이러한 라이브러리에는 모두 Starlette 전용 가이드가 있어 이를 사용할 수 있다.

- [Ariadne](https://ariadnegraphql.org/docs/starlette-integration.html)
- [`starlette-graphene3`](https://github.com/ciscorn/starlette-graphene3#example)
- [Strawberry](https://strawberry.rocks/docs/integrations/starlette)
- [`tartiflette-asgi`](https://tartiflette.github.io/tartiflette-asgi/usage/#starlette)
