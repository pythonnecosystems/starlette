# 소개

Starlette는 Python으로 비동기 웹 서비스를 구축하는 데 이상적인 경량 [ASGI](https://asgi.readthedocs.io/en/latest/) 프레임워크/툴킷이다.

프로덕션에 바로 사용할 수 있으며 다음과 같은 특징이 있다.

- 가볍고 복잡하지 않은 HTTP 웹 프레임워크
- WebSocket 지원
- 진행 중인 백그라운드 작업
- 시작과 종료 이벤트
- `httpx`에 구축된 테스트 클라이언트
- CORS, GZip, 정적 파일, 스트리밍 응답
- 세션과 쿠키 지원
- 100% 테스트 커버리지
- 100% 타입 어노테이트된 코드베이스
- 하드 종속성이 거의 없음
- `ayncio`와 `trio` 백엔드와 호환
- [독립적인 벤치마크](https://www.techempower.com/benchmarks/#hw=ph&test=fortune&l=zijzen-sf)에서 전반적으로 뛰어난 성능

## 요구사항
Python 3.8+

## 설치

```bash
$ pip install starlette
```

또한 [uvicorn](http://www.uvicorn.org/), [daphne](https://github.com/django/daphne/) 또는 [hypercorn](https://pgjones.gitlab.io/hypercorn/)과 같은 ASGI 서버를 설치하는 것을 권장한다.

```bash
$ pip install uvicorn
```

## 예

**example.py**

```python
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Route


async def homepage(request):
    return JSONResponse({'hello': 'world'})


app = Starlette(debug=True, routes=[
    Route('/', homepage),
])
```

어플리케이션을 실행한다.

```bash
$ uvicorn example:app
```

[여기](https://github.com/encode/starlette-example)에서 더 많은 예를 찾아 볼 수 있다.

## 의존성
Starlette에는 `anyio`이 필요하며 다음 의존성은 선택 사항이다.

- [httpx](https://www.python-httpx.org/) - `TestClient`를 사용하려는 경우 필수이다.
- [jinja2](https://jinja.palletsprojects.com/) - `Jinja2Templates`를 사용하려는 경우 필수이다.
- [python-multipart](https://andrew-d.github.io/python-multipart/) - `request.form()`으로 양식 구문 분석을 지원하려면 필요하다.
- [itsdangerous](https://pythonhosted.org/itsdangerous/) - `SessionMiddleware` 지원을 위해 필요하다.
- [pyyaml](https://pyyaml.org/wiki/PyYAMLDocumentation) - `SchemaGenerator` 지원을 위해 필요하다.

`pip3 install starlette[full]`로 모두 설치할 수 있다.

## 프레임워크 또는 툴키트
Starlette는 완전한 프레임워크 또는 ASGI 툴킷으로 사용할 수 있도록 설계되었다. 모든 구성 요소를 독립적으로 사용할 수 있다.

```python
from starlette.responses import PlainTextResponse


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = PlainTextResponse('Hello, world!')
    await response(scope, receive, send)
```

`example.py`내의 `app` 어플리케이션을 실행한다.

```bash
$ uvicorn example:app
INFO: Started server process [11509]
INFO: Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

코드 변경 시 자동으로 다시 로드하도록 하려면 `--reload` 선택 사항을 사용하여 uvicorn을 실행한다.

## 모듈화
모듈화로 설계된 Starlette는 ASGI 프레임워크 간에 공유할 수 있는 재사용 가능한 구성 요소를 구축하는 것을 장려한다. 이를 통해 공유 미들웨어와 마운트 가능한 어플리케이션으로 구성된 에코시스템을 구축할 수 있다.

또한 깔끔한 API 분리는 각 구성 요소를 개별적으로 더 쉽게 이해할 수 있음을 의미한다.
