Starlette 어플리케이션은 어플리케이션이 시작되기 전 또는 어플리케이션이 종료될 때 실행해야 하는 코드를 처리하기 위해 수명 처리기를 등록할 수 있다.

```python
import contextlib

from starlette.applications import Starlette


@contextlib.asynccontextmanager
async def lifespan(app):
    async with some_async_resource():
        print("Run at startup!")
        yield
        print("Run on shutdown!")


routes = [
    ...
]

app = Starlette(routes=routes, lifespan=lifespan)
```

Starlette은 수명이 실행될 때까지 들어오는 요청에 대한 서비스를 시작하지 않는다.

모든 연결이 닫히고 진행 중인 모든 백그라운드 작업이 완료되면 수명 해체가 실행된다.

비동기 작업을 관리하려면 `anyio.create_task_group()`을 사용하는 것을 고려하자.

## 수명 상태
수명에는 `state`라는 개념이 있는데, 이는 수명과 요청 간에 객체를 공유하는 데 사용할 수 있는 딕셔너리이다.

```python
import contextlib
from typing import AsyncIterator, TypedDict

import httpx
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import PlainTextResponse
from starlette.routing import Route


class State(TypedDict):
    http_client: httpx.AsyncClient


@contextlib.asynccontextmanager
async def lifespan(app: Starlette) -> AsyncIterator[State]:
    async with httpx.AsyncClient() as client:
        yield {"http_client": client}


async def homepage(request: Request) -> PlainTextResponse:
    client = request.state.http_client
    response = await client.get("https://www.example.com")
    return PlainTextResponse(response.text)


app = Starlette(
    lifespan=lifespan,
    routes=[Route("/", homepage)]
)
```

요청에 수신된 `state`는 수명 처리기에 수신된 상태의 **얕은** 복사본이다.

## <a name="running-lifespan-in-tests"></a>테스트에서 실행 수명
`TestClient`를 컨텍스트 관리자로 사용하여 수명이 호출되는지 확인해야 한다.

```python
from example import app
from starlette.testclient import TestClient


def test_homepage():
    with TestClient(app) as client:
        # Application's lifespan is called on entering the block.
        response = client.get("/")
        assert response.status_code == 200

    # And the lifespan's teardown is run when exiting the block.
```
