Starlette에는 전체 어플리케이션에 적용되는 동작을 추가하기 위한 여러 미들웨어 클래스가 포함되어 있다. 이러한 클래스는 모두 표준 ASGI 미들웨어 클래스로 구현되며 Starlette 또는 다른 ASGI 어플리케이션에 모두 적용할 수 있다.

## <a name="using-middleware">미들웨어 사용</a>
Starlette 어플리케이션 클래스를 사용하면 예외 처리기에 의해 래핑된 상태로 유지되는 방식으로 ASGI 미들웨어를 포함할 수 있다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.httpsredirect import HTTPSRedirectMiddleware
from starlette.middleware.trustedhost import TrustedHostMiddleware

routes = ...

# Ensure that all requests include an 'example.com' or
# '*.example.com' host header, and strictly enforce https-only access.
middleware = [
    Middleware(
        TrustedHostMiddleware,
        allowed_hosts=['example.com', '*.example.com'],
    ),
    Middleware(HTTPSRedirectMiddleware)
]

app = Starlette(routes=routes, middleware=middleware)
```

모든 Starlette 어플리케이션에는 기본적으로 두 가지 미들웨어가 자동으로 포함된다.

- `ServerErrorMiddleware` - 어플리케이션 예외가 커스텀 500 페이지를 반환하거나 DEBUG 모드에서 어플리케이션 트레이스백을 표시할 수 있도록 한다. 이 미들웨어는 항상 가장 바깥쪽에 있는 미들웨어 계층이다.
- `ExceptionMiddleware` - 예외 처리기를 추가하여 특정 타입의 예상되는 예외 사례를 처리기 함수에 연결할 수 있도록 한다. 예를 들어 엔드포인트 내에서 `HTTPException(status_code=404)`을 발생시키면 커스텀 404 페이지가 렌더링된다.

미들웨어는 위에서 아래로 평가되므로 예제 어플리케이션의 실행 흐름은 다음과 같다.

- 미들웨어
    - `ServerErrorMiddleware`
    - `TrustedHostMiddleware`
    - `HTTPSRedirectMiddleware`
    - `ExceptionMiddleware`
- 라우팅
- 엔드포인트

Starlette 패키지에서 사용할 수 있는 미들웨어 구현은 다음과 같다.

## CORSMiddleware
브라우저에서 교차 출처 요청을 허용하기 위해 발신 응답에 적절한 [CORS headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)를 추가한다.

CORSMiddleware 구현에서 사용하는 기본 매개변수는 기본적으로 제한적이므로 브라우저에서 Cross-Domain 컨텍스트에서 특정 오리진, 메서드 또는 헤더를 사용하도록 허용하려면 명시적으로 사용하도록 설정해야 한다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

routes = ...

middleware = [
    Middleware(CORSMiddleware, allow_origins=['*'])
]

app = Starlette(routes=routes, middleware=middleware)
```

다음 인수가 지원된다.

- `allow_origins` - 교차 출처(cross-origin) 요청을 허용해야 하는 출처 목록이다. 예: `['https://example.org', 'https://www.example.org']`. `['*']`를 사용하여 모든 출처를 허용할 수 있다.
- `allow_origin_regex` - 교차 출처 요청을 허용해야 하는 출처와 일치시킬 정규식 문자열이다(예: `'https://.*\.example\.org'`).
- `allow_methods` - 교차 출처 요청에 허용해야 하는 HTTP 메소드 목록이다. 기본값은 `['GET']`이다. 모든 표준 메소드를 허용하려면 `['*']`를 사용할 수 있다.
- `allow_headers` - 교차 출처 요청에 대해 지원해야 하는 HTTP 요청 헤더 목록이다. 기본값은 `[]`이다. 모든 헤더를 허용하려면 `['*']`를 사용할 수 있다. `Accept`, `Accept-Language`, `Content-Language`와 `Content-Type` 헤더는 항상 CORS 요청에 허용된다.
- `allow-credentials` - 교차 출처 요청에 대해 쿠키를 지원해야 함을 나타낸다. 기본값은 `False`이다.
- `expose_headers` - 브라우저에서 액세스할 수 있도록 해야 하는 모든 응답 헤더를 표시한다. 기본값은 `[]`이다.
- `max_age` - 브라우저가 CORS 응답을 캐시할 수 있는 최대 시간(초)을 설정한다. 기본값은 `600`이다.

미들웨어는 두 가지 특정 타입의 HTTP 요청에 응답한다...

### CORS 비행전(preflight) 요청
`Origin`과 `Access-Control-Request-Method` 헤더가 있는 모든 `OPTIONS` 요청이다. 이 경우 미들웨어는 들어오는 요청을 가로채서 적절한 CORS 헤더와 정보 제공을 위한 200 또는 400 응답으로 응답한다.

### 단순 요청
`Origin` 헤더가 있는 모든 요청. 이 경우 미들웨어는 정상적으로 요청을 통과시키지만 응답에 적절한 CORS 헤더를 포함한다.

## SessionMiddleware
서명된 쿠키 기반 HTTP 세션을 추가한다. 세션 정보는 읽을 수 있지만 수정할 수는 없다.

`request.session` 딕셔너리 인터페이스를 사용하여 세션 데이터를 액세스하거나 수정한다.

다음 인수가 지원된다.

- `secret_key` - 임의의 문자열이어야 한다.
- `session_cookie` - 기본값은 "session"이다.
- `max_age` - 세션 만료 시간(초)이다. 기본값은 2주이다. `None`으로 설정하면 쿠키는 브라우저 세션만큼 오래 지속된다.
- `same_site` - Samesite 플래그는 브라우저가 교차 사이트 요청과 함께 세션 쿠키를 보내지 않도록 한다. 기본값은 `'lax'`이다.
- `https_only` - Secure 플래그를 설정해야 함을 나타낸다 (HTTPS에서만 사용 가능). 기본값은 `False`이다.

## HTTPSRedirectMiddleware
들어오는 모든 요청은 `https` 또는 `wss`여야 한다. `http` 또는 `ws`로 들어오는 모든 요청은 대신 보안 체계로 리디렉션된다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.httpsredirect import HTTPSRedirectMiddleware

routes = ...

middleware = [
    Middleware(HTTPSRedirectMiddleware)
]

app = Starlette(routes=routes, middleware=middleware)
```

이에 대한 구성 옵션은 없다.

## TrustedHostMiddleware
들어오는 모든 요청에 `Host` 헤더가 정확하하게 설정되어 있는지 확인하여 HTTP Host Header 공격을 방지한다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.trustedhost import TrustedHostMiddleware

routes = ...

middleware = [
    Middleware(TrustedHostMiddleware, allowed_hosts=['example.com', '*.example.com'])
]

app = Starlette(routes=routes, middleware=middleware)
```

지원되는 인수는 다음과 같다.

- `allowed_hosts` -hostname으로 허용되는 도메인 이름 목록이다. 일치하는 하위 도메인에 `*.example.com`과 같은 와일드카드 도메인을 지원한다. 모든 호스트 이름을 허용하려면 `allowed_hosts=["*"]`를 사용하거나 미들웨어를 생략한다.

들어오는 요청의 유효성을 정확히 검사하지 않으면 400 응답이 전송된다.

## GZipMiddleware
`Accept-Encoding` 헤더에 `"gzip"`이 포함된 모든 요청에 대해 GZip 응답을 처리한다.

미들웨어는 표준 응답과 스트리밍 응답을 모두 처리한다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.gzip import GZipMiddleware


routes = ...

middleware = [
    Middleware(GZipMiddleware, minimum_size=1000)
]

app = Starlette(routes=routes, middleware=middleware)
```

지원되는 인수는 다음과 같다.

- `minimum_size` - 이 최소 크기(바이트)보다 작은 응답을 GZip하지 않는다. 기본값은 `500`이다.
미들웨어는 `Content-Encoding`이 이미 설정된 응답을 두 번 인코딩하는 것을 방지하기 위해 응답을 GZip하지 않는다.

## BaseHTTPMiddleware
요청/응답 인터페이스에 대해 ASGI 미들웨어를 작성할 수 있는 추상 클래스이다.

### 사용
`BaseHTTPMiddleware`를 사용하여 미들웨어 클래스를 구현하려면 `async def dispatch(request, call_next)` 메서드를 재정의(override)해야 한다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.base import BaseHTTPMiddleware


class CustomHeaderMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        response = await call_next(request)
        response.headers['Custom'] = 'Example'
        return response

routes = ...

middleware = [
    Middleware(CustomHeaderMiddleware)
]

app = Starlette(routes=routes, middleware=middleware)
```

미들웨어 클래스에 구성 옵션을 제공하려면 `__init__` 메서드를 재정의하여 첫 번째 인수가 `app`이고 나머지 인수가 선택적 키워드 인자인지 확인해야 한다. 이 경우 인스턴스에서 `app` 속성을 설정해야 한다.

```python
class CustomHeaderMiddleware(BaseHTTPMiddleware):
    def __init__(self, app, header_value='Example'):
        super().__init__(app)
        self.header_value = header_value

    async def dispatch(self, request, call_next):
        response = await call_next(request)
        response.headers['Custom'] = self.header_value
        return response


middleware = [
    Middleware(CustomHeaderMiddleware, header_value='Customized')
]

app = Starlette(routes=routes, middleware=middleware)
```

미들웨어 클래스는 `__init__` 메서드 외부에서 상태를 수정해서는 안 된다. 대신 미들웨어 인스턴스를 변경하는 대신 상태를 `dispatch` 메서드에 로컬로 유지하거나 명시적으로 전달해야 한다.

### 제한 사항
현재 `BaseHTTPMiddleware`에는 몇몇 알려진 제한 사항이 있다.

- `BaseHTTPMiddleware`를 사용하면 [`contextlib.ContextVar`](https://docs.python.org/3/library/contextvars.html#contextvars.ContextVar)에 대한 변경 사항이 위쪽으로 전파되지 않는다. 즉, 엔드포인트에서 `ContextVar`에 대한 값을 설정하고 미들웨어에서 읽으려고 하면 엔드포인트에서 설정한 값과 동일한 값이 아니라는 것을 알 수 있다 (이 동작의 예는 [이 테스트](https://github.com/encode/starlette/blob/621abc747a6604825190b93467918a0ec6456a24/tests/middleware/test_base.py#L192-L223)를 참조하자).

이러한 한계를 극복하려면 아래 [순수 ASGI 미들웨어]()를 사용하세요.

## 순수 ASGI 미들웨어
[ASGI 사양](https://asgi.readthedocs.io/en/latest/)을 사용하면 ASGI 인터페이스를 직접 사용하여 다음 ASGI 어플리케이션으로 호출하는 ASGI 미들웨어를 구현할 수 있다. 실제로 Starlette와 함께 제공되는 미들웨어 클래스는 이러한 방식으로 구현된다.

이 낮은 수준의 접근 방식은 동작을 더 잘 제어하고 프레임워크와 서버 간에 상호 운용성을 향상시킨다. 또한 `BaseHTTPMiddleware`의 한계를 극복한다.

### 순수 ASGI 미들웨어 작성
ASGI 미들웨어를 생성하는 가장 일반적인 방법은 클래스를 사용하는 것이다.

```python
class ASGIMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        await self.app(scope, receive, send)
```

위의 미들웨어는 가장 기본적인 ASGI 미들웨어이다. 이 미들웨어는 생성자의 인수로 상위 ASGI 어플리케이션을 받고, 해당 상위 어플리케이션으로 호출하는 비동기 `__call__` 메서드를 구현한다.

`asgi-cors`와 같은 일부 구현은 함수를 사용하는 대체 스타일을 사용한다.

```python
import functools

def asgi_middleware():
    def asgi_decorator(app):

        @functools.wraps(app)
        async def wrapped_app(scope, receive, send):
            await app(scope, receive, send)

        return wrapped_app

    return asgi_decorator
```

어떤 경우든 ASGI 미들웨어는 `scope`, `receive`와 `send`의 세 인수를 허용하는 콜러블이어야 한다.

- `scope`는 연결에 대한 정보를 담고 있는 딕셔너리로, `scope["type"]`은 아래와 같을 수 있다.
    - `"http"`: HTTP 요청용.
    - `"websocket"`: 웹소켓 연결용.
    - `"lifespan"`: ASGI 수명 메시지용.
- `receive`와 `send`를 사용하여 ASGI 서버와 ASGI 이벤트 메시지를 교환할 수 있다 (자세한 내용은 아래 참조). 이러한 메시지의 타입과 내용은 범위 타입에 따라 다fm다. 자세한 내용은 [ASGI 사양](https://asgi.readthedocs.io/en/latest/specs/index.html)에서 확인할 수 있다.

### 순수 ASGI 미들웨어 사용
순수 ASGI 미들웨어는 다른 미들웨어와 마찬가지로 사용할 수 있다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware

from .middleware import ASGIMiddleware

routes = ...

middleware = [
    Middleware(ASGIMiddleware),
]

app = Starlette(..., middleware=middleware)
```

[미들웨어 사용](#using-middleware)을 참조하시오.

### 타입 어노테이션
미들웨어에 어노테이션을 하는 방법에는 Starlette 자체 또는 [asgiref](https://github.com/django/asgiref)를 사용하는 두 가지 방법이 있다.

- Starlette 사용: 가장 일반적인 사용 사례

```python
from starlette.types import ASGIApp, Message, Scope, Receive, Send


class ASGIMiddleware:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] != "http":
            return await self.app(scope, receive, send)

        async def send_wrapper(message: Message) -> None:
            # ... Do something
            await send(message)

        await self.app(scope, receive, send_wrapper)
```

- [asgiref](https://github.com/django/asgiref) 사용: 보다 엄격한 타입 힌트 제공.

```python
from asgiref.typing import ASGI3Application, ASGIReceiveCallable, ASGISendCallable, Scope
from asgiref.typing import ASGIReceiveEvent, ASGISendEvent


class ASGIMiddleware:
    def __init__(self, app: ASGI3Application) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: ASGIReceiveCallable, send: ASGISendCallable) -> None:
        if scope["type"] != "http":
            await self.app(scope, receive, send)
            return

        async def send_wrapper(message: ASGISendEvent) -> None:
            # ... Do something
            await send(message)

        return await self.app(scope, receive, send_wrapper)
```

### 일반적인 패턴

#### 특정 요청만 처리
ASGI 미들웨어는 `scope`의 내용에 따라 특정 작업을 적용할 수 있다.

예를 들어 HTTP 요청만 처리하려면 다음과 같이 작성한다.

```python
class ASGIMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            await self.app(scope, receive, send)
            return

        ...  # Do something here!

        await self.app(scope, receive, send)
```

마찬가지로 WebSocket 전용 미들웨어는 `scope["type"] != "websocket"에 대해 막는다.

미들웨어는 request 메소드, URL, header 등에 따라 다르게 작동할 수도 있다.

#### Starlette 구성요소 재활용
Starlette는 더 높은 수준의 추상화 작업을 할 수 있도록 ASGI `scope`, `receive` 와/또는 `send` 인수를 를 허용하는 데이터 구조를 제공한다. 이러한 데이터 구조에는 [`Request`](./requests.md#request), [`Headers`](./requests.md#headers), [`QueryParams`](./requests.md#query-parameters), [`URL`](./requests.md#url) 등이 포함된다.

예를 들어 `Request`를 인스턴스화하여 HTTP 요청을 더 쉽게 검사할 수 있다.

```python
from starlette.requests import Request

class ASGIMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        if scope["type"] == "http":
            request = Request(scope)
            ... # Use `request.method`, `request.url`, `request.headers`, etc.

        await self.app(scope, receive, send)
```

ASGI 어플리케이션인 응답을 재사용할 수도 있다.

#### 열성적인 응답 발신
연결 `scope`를 검사하면 조건부로 다른 ASGI 앱으로 호출할 수 있다. 한 가지 사용 사례는 앱을 호출하지 않고 응답을 보내는 것이다.

예를 들어, 이 미들웨어는 딕셔너리을 사용하여 요청된 경로에 따라 영구 리디렉션을 수행한다. 이는 경로 URL 패턴을 리팩터링해야 하는 경우 레거시 URL에 대한 지속적인 지원을 구현하는 데 사용할 수 있다.

```python
from starlette.datastructures import URL
from starlette.responses import RedirectResponse

class RedirectsMiddleware:
    def __init__(self, app, path_mapping: dict):
        self.app = app
        self.path_mapping = path_mapping

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            await self.app(scope, receive, send)
            return

        url = URL(scope=scope)

        if url.path in self.path_mapping:
            url = url.replace(path=self.path_mapping[url.path])
            response = RedirectResponse(url, status_code=301)
            await response(scope, receive, send)
            return

        await self.app(scope, receive, send)
```

사용 예는 다음과 같다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware

routes = ...

redirections = {
    "/v1/resource/": "/v2/resource/",
    # ...
}

middleware = [
    Middleware(RedirectsMiddleware, path_mapping=redirections),
]

app = Starlette(routes=routes, middleware=middleware)
```

#### 요청 검사 또는 변경
`scope`를 조작하여 요청 정보에 액세스하거나 변경할 수 있다. 이 패턴의 전체 예는 프론트엔드 프록시 뒤에서 서비스할 때 `scope`를 검사하고 조정하는 Uvicorn의 `ProxyHeadersMiddleware`를 참조하시오.

또한 `receive` ASGI 콜러블을 래핑하면 `http.request` ASGI 이벤트 메시지를 조작하여 HTTP 요청 본문을 액세스하거나 수정할 수 있다.

예를 들어, 이 미들웨어는 수신하는 요청 본문의 크기를 계산하고 기록한다...

```python
class LoggedRequestBodySizeMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            await self.app(scope, receive, send)
            return

        body_size = 0

        async def receive_logging_request_body_size():
            nonlocal body_size

            message = await receive()
            assert message["type"] == "http.request"

            body_size += len(message.get("body", b""))

            if not message.get("more_body", False):
                print(f"Size of request body was: {body_size} bytes")

            return message

        await self.app(scope, receive_logging_request_body_size, send)
```

마찬가지로 WebSocket 미들웨어는 수신되는 WebSocket 데이터를 검사하거나 변경하기 위해 [`websocket.receive`](https://asgi.readthedocs.io/en/latest/specs/www.html#receive-receive-event) ASGI 이벤트 메시지를 조작할 수 있다.

HTTP 요청 본문을 변경하는 예는 [`msgpack-asgi`](https://github.com/florimondmanca/msgpack-asgi)를 참조하시오.

#### 응답 검사 또는 변경
`send` ASGI 콜러블을 래핑하면 기본 어플리케이션에서 보낸 HTTP 응답을 검사하거나 수정할 수 있다. 이렇게 하려면 [`http.response.start`](https://asgi.readthedocs.io/en/latest/specs/www.html#response-start-send-event) 또는 [`http.response.body`](https://asgi.readthedocs.io/en/latest/specs/www.html#response-body-send-event) ASGI 이벤트 메시지에 반응하면 된다.

예를 들어, 이 미들웨어는 몇 가지 고정된 추가 응답 헤더를 추가한다.

```python
from starlette.datastructures import MutableHeaders

class ExtraResponseHeadersMiddleware:
    def __init__(self, app, headers):
        self.app = app
        self.headers = headers

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            return await self.app(scope, receive, send)

        async def send_with_extra_headers(message):
            if message["type"] == "http.response.start":
                headers = MutableHeaders(scope=message)
                for key, value in self.headers:
                    headers.append(key, value)

            await send(message)

        await self.app(scope, receive, send_with_extra_headers)
```

HTTP 응답을 검사하고 구성 가능한 HTTP 액세스 로그 행을 기록하는 예는 [asgi-logger](https://github.com/Kludex/asgi-logger/blob/main/asgi_logger/middleware.py)를 참조하시오.

마찬가지로 WebSocket 미들웨어는 보내는 WebSocket 데이터를 검사하거나 변경하기 위해 [`websocket.send`](https://asgi.readthedocs.io/en/latest/specs/www.html#send-send-event) ASGI 이벤트 메시지를 조작할 수 있다.

응답 본문을 변경하는 경우 새 응답 본문 길이와 일치하도록 응답 `Content-Length` 헤더를 업데이트해야 한다는 점에 유의하자. 전체 예는 [brotli-asgi](https://github.com/fullonic/brotli-asgi)를 참조하시오.

#### 엔트포인트로 정보 전달
기본 앱 또는 엔드포인트와 정보를 공유해야 하는 경우 `scope` 딕셔너리에 정보를 저장할 수 있다. 이는 관례(예: Starlette는 이를 사용하여 엔드포인트와 라우팅 정보를 공유)이지만 ASGI 사양의 일부가 아니라는 점에 유의하자. 이 경우 다른 미들웨어나 어플리케이션에서 사용할 가능성이 낮은 키를 사용하여 충돌을 피해야 한다.

예를 들어, 아래 미들웨어를 포함할 경우 엔드포인트는 `request.scope["asgi_transaction_id"]`를 액세스할 수 있다.

```python
import uuid

class TransactionIDMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        scope["asgi_transaction_id"] = uuid.uuid4()
        await self.app(scope, receive, send)
```

#### 정리 및 오류 처리
어플리케이션을 `try/except/finally` 블록 또는 컨텍스트 관리자로 래핑하여 정리 작업을 수행하거나 오류 처리를 수행할 수 있다.

예를 들어, 다음 미들웨어는 메트릭을 수집하고 어플리케이션 예외를 처리할 수 있다...

```python
import time

class MonitoringMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        start = time.time()
        try:
            await self.app(scope, receive, send)
        except Exception as exc:
            ...  # Process the exception
            raise
        finally:
            end = time.time()
            elapsed = end - start
            ...  # Submit `elapsed` as a metric to a monitoring backend
```

이 패턴 전체 예는 [timing-asgi](https://github.com/steinnes/timing-asgi)를 참조하시오.

### Gotchas

#### ASGI 미들웨어는 상태를 저장하지 않아야 한다
ASGI는 동시 요청을 처리하도록 설계되었으므로 모든 연결별 상태는 `__call__` 구현으로 범위를 한정해야 한다. 그렇게 하지 않으면 일반적으로 요청 간에 변수 읽기/쓰기가 충돌하고 버그가 발생할 가능성이 높다.

예를 들어 응답에 `X-Mock` 헤더가 있는 경우 응답 본문을 조건부로 대체한다...

- Do

```python
from starlette.datastructures import Headers

class MockResponseBodyMiddleware:
    def __init__(self, app, content):
        self.app = app
        self.content = content

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            await self.app(scope, receive, send)
            return

        # A flag that we will turn `True` if the HTTP response
        # has the 'X-Mock' header.
        # ✅: Scoped to this function.
        should_mock = False

        async def maybe_send_with_mock_content(message):
            nonlocal should_mock

            if message["type"] == "http.response.start":
                headers = Headers(raw=message["headers"])
                should_mock = headers.get("X-Mock") == "1"
                await send(message)

            elif message["type"] == "http.response.body":
                if should_mock:
                    message = {"type": "http.response.body", "body": self.content}
                await send(message)

        await self.app(scope, receive, maybe_send_with_mock_content)
```

- Don't

```python
from starlette.datastructures import Headers

class MockResponseBodyMiddleware:
    def __init__(self, app, content):
        self.app = app
        self.content = content
        # ❌: This variable would be read and written across requests!
        self.should_mock = False

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            await self.app(scope, receive, send)
            return

        async def maybe_send_with_mock_content(message):
            if message["type"] == "http.response.start":
                headers = Headers(raw=message["headers"])
                self.should_mock = headers.get("X-Mock") == "1"
                await send(message)

            elif message["type"] == "http.response.body":
                if self.should_mock:
                    message = {"type": "http.response.body", "body": self.content}
                await send(message)

        await self.app(scope, receive, maybe_send_with_mock_content)
```

이 잠재적인 문제를 해결할 수 있는 전체 구현 예는 [GZipMiddleware](https://github.com/encode/starlette/blob/9ef1b91c9c043197da6c3f38aa153fd874b95527/starlette/middleware/gzip.py)를 참조하시오.

### 추가 읽기
이 문서만으로도 ASGI 미들웨어를 만드는 방법에 대한 좋은 기초를 갖추기에 충분할 것이다.

그럼에도 불구하고 이 주제에 대한 훌륭한 아티클들이 있다.

- [Introduction to ASGI: Emergence of an Async Python Web Ecosystem](https://florimond.dev/en/posts/2019/08/introduction-to-asgi-async-python-web/)
- [How to write ASGI middleware](https://pgjones.dev/blog/how-to-write-asgi-middleware-2021/)

## 다른 프레임워크에서 미들웨어 사용
다른 ASGI 어플리케이션에 ASGI 미들웨어를 래핑하려면, 어플리케이션 인스턴스를 래핑하는 보다 일반적인 패턴을 사용해야 한다.

```python
    app = TrustedHostMiddleware(app, allowed_hosts=['example.com'])
```

Starlette 어플리케이션 인스턴스에서도 이 작업을 수행할 수 있지만, `middleware=<List of Middleware instances>` 스타일을 사용하는 것이 바람직하다.

- 모든 것이 하나의 가장 바깥쪽 `ServerErrorMiddleware`에 래핑된 상태로 유지되도록 한다.
- 최상위 `app` 인스턴스를 유지한다.

## 미들웨어를 `Mount` S에 적용
`Mount`에도 미들웨어를 추가할 수 있으며, 이를 통해 단일 경로, 경로 그룹 또는 마운트된 모든 ASGI 어플리케이션에 미들웨어를 적용할 수 있다.

```python
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.gzip import GZipMiddleware
from starlette.routing import Mount, Route


routes = [
    Mount(
        "/",
        routes=[
            Route(
                "/example",
                endpoint=...,
            )
        ],
        middleware=[Middleware(GZipMiddleware)]
    )
]

app = Starlette(routes=routes)
```

이러한 방식으로 사용되는 미들웨어는 `Starlette` 어플리케이션에 적용된 미들웨어처럼 예외 처리 미들웨어로 래핑되지 않는다는 점에 유의하자. 이는 응답을 검사하거나 수정하는 미들웨어에만 적용되므로 문제가 되지 않는 경우가 많으며, 오류 응답에 이 로직을 적용하고 싶지 않을 수도 있다. 일부 경로에서만 미들웨어 로직을 오류 응답에 적용하려는 경우 몇 가지 옵션이 있다.

- `Mount`에 `ExceptionMiddleware`를 추가한다.
- 미들웨어에 `try/except` 블록을 추가하고 거기에서 오류 응답을 반환한다.
- 마킹과 처리를 두 개의 미들웨어로 분할하여 하나는 응답을 처리가 필요한 것으로 표시하는 `Mount` (예: `scope["log-response"] = True` 설정)에 배치하고, 다른 하나는 무거운 작업을 수행하는 `Starlette` 어플리케이션에 적용한다.

## 서드 파티 미들웨어

### [asgi-auth-github](https://github.com/simonw/asgi-auth-github)
이 미들웨어는 모든 ASGI 어플리케이션에 인증을 추가하여 사용자가 GitHub 계정을 사용하여 로그인하도록 요구한다(`OAuth`를 통해). 특정 사용자 또는 특정 GitHub 조직 또는 팀의 구성원으로 액세스를 제한할 수 있다.

### [asgi-csrf](https://github.com/simonw/asgi-csrf)
CSRF 공격으로부터 보호하기 위한 미들웨어. 이 미들웨어는 쿠키가 설정된 후 csrftoken 숨겨진 양식 필드 또는 `x-csrftoken` HTTP 헤더와 비교되는 Double Submit Cookie 패턴을 구현한다.

### [AuthlibMiddleware](https://github.com/aogier/starlette-authlib)
`authlib's jwt` 모듈을 사용하여 Starlette 세션 미들웨어를 대체하는 드롭인(drop-in)이다.

### [BugsnagMiddleware](https://github.com/ashinabraham/starlette-bugsnag)
[Bugsnag](https://www.bugsnag.com/)에 예외를 로깅하기 위한 미들웨어 클래스이다.

### [CSRFMiddleware](https://github.com/frankie567/starlette-csrf)
CSRF 공격으로부터 보호하기 위한 미들웨어. 이 미들웨어는 쿠키를 설정한 다음 `x-csrftoken` HTTP 헤더와 비교하는 Double Submit Cookie 패턴을 구현한다.

### [EarlyDataMiddleware](https://github.com/HarrySky/starlette-early-data)
[TLSv1.3 early data](https://tools.ietf.org/html/rfc8470) 요청을 감지하고 거부하기 위한 미들웨어와 데코레이터이다.

### [PrometheusMiddleware](https://github.com/perdy/starlette-prometheus)
진행 중인 요청, 타이밍을 포함하여 요청 및 응답과 관련된 Prometheus 메트릭을 캡처하기 위한 미들웨어 클래스...

### [ProxyHeadersMiddleware](https://github.com/encode/uvicorn/blob/master/uvicorn/middleware/proxy_headers.py)
Uvicorn에는 프록시 서버가 사용되는 경우 `X-Forwarded-Proto`와 `X-Forwarded-For` 헤더를 기반으로 클라이언트 IP 주소를 결정하기 위한 미들웨어 클래스가 포함되어 있다. 보다 복잡한 프록시 구성의 경우 이 미들웨어를 적용할 수 있다.

### [RateLimitMiddleware](https://github.com/abersheeran/asgi-ratelimit)
평가 제한 미들웨어. 정규식을 URL, 유연한 규칙에 일치시킨다. 고도로 커스터마이징할 수 있다. 사용하기 매우 쉽다.

### [RequestIdMiddleware](https://github.com/snok/asgi-correlation-id)
요청 ID를 읽거나 생성하여 어플리케이션 로그에 첨부하기 위한 미들웨어 클래스이다.

### [RollbarMiddleware](https://docs.rollbar.com/docs/starlette)
예외, 오류 및 로그 메시지를 [Rollbar](https://www.rollbar.com/)에 기록하기 위한 미들웨어 클래스이다.

### [StarletteOpentracing](https://github.com/acidjunk/starlette-opentracing)
추적 정보를 [OpenTracing.io](https://opentracing.io/) 호환 추적기로 전송하고 분산 어플리케이션을 프로파일링과 모니터링하는 데 사용할 수 있는 미들웨어 클래스이다.

### [SecureCookiesMiddleware](https://github.com/thearchitector/starlette-securecookies)
기존 쿠키 기반 미들웨어에 대한 추가 지원과 함께 Starlette 어플리케이션에 자동 쿠키 암호화 및 복호화를 추가할 수 있도록 커스터마이징할 수 있는 미들웨어이다.

### [TimingMiddleware](https://github.com/steinnes/timing-asgi)
미들웨어 클래스를 통과하는 각 요청에 대해 타이밍 정보(CPU와 월 시간)를 방출하는 미들웨어 클래스이다. 이러한 타이밍을 통계 지표로 내보내는 방법에 대한 예제가 포함되어 있다.

### [WSGIMiddleware](https://github.com/abersheeran/a2wsgi)
WSGI 어플리케이션을 ASGI 어플리케이션으로 변환하는 작업을 담당하는 미들웨어 클래스이다.
