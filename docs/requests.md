Starlette에는 ASGI 스코프와 수신 채널에 직접 액세스하는 대신 들어오는 요청에 대해 더 나은 인터페이스를 제공하는 `Request` 클래스가 포함되어 있다.

## <a name="reauest">Request</a>
Signature: `Response(scope, receive=None)`

```python
from starlette.requests import Request
from starlette.responses import Response


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    request = Request(scope, receive)
    content = '%s %s' % (request.method, request.url.path)
    response = Response(content, media_type='text/plain')
    await response(scope, receive, send)
```

요청(Requests)은 매핑 인터페이스를 제공하므로 `scope`와 같은 방식으로 사용할 수 있다.

예를 들어: `request['path']`는 ASGI 경로를 반환한다.

요청 본문에 액세스할 필요가 없는 경우 `receive`에 인수를 제공하지 않고 요청을 인스턴스화할 수 있다.

### Method
요청 메서드를 `request.method`로 액세스할 수 있다.

### <a name="url">URL</a>
요청 URL을 `request.url`로 액세스한다.

이 속성은 URL에서 구문 분석할 수 있는 모든 구성 요소를 노출하는 문자열 같은 객체이다.

예: `reuest.url.path`, `reauest.url.port`, `request.url.scheme`.

### <a name="headers">헤더(Headers)</a>
헤더는 대소문자를 구분하지 않는 변경할 수 없는 다중 딕셔너리로 노출된다.

예: `request.headers['content-type']`

### <a nmae="query-parameters">쿼리 매개변수(Query Parameters)</a>
쿼리 매개변수는 변경할 수 없는 다중 딕셔너리로 노출된다.

예: `request.query_params['search']`

### 경로 매개변수
라우터 경로 매개변수는 사전 인터페이스로 노출된다.

예: `request.path_params['username']`

### 클라이언트 주소 (Client Address)
클라이언트의 원격 주소는 명명된 2차원 튜플 `request.client`로 노출된다. 튜플의 항목 중 하나는 None일 수 있다.

호스트 이름 또는 IP 주소: `request.client.host`

클라이언트가 연결하는 포트 번호: `request.client.port`

### 쿠키
쿠키는 일반 사전 인터페이스로 노출된다.

예: `request.cookies.get('mycookie')`

쿠키가 유효하지 않은 경우 쿠키는 무시된다. (RFC2109)

### 바디(body)
요청 본문을 반환하는 다른 인터페이스가 몇몇 있다.

바이트 형식의 요청 본문: `await request.body()`

양식 데이터 또는 다중 파트로 구문 분석된 요청 본문: `async with request.form() as form:`

JSON으로 구문 분석된 요청 본문: `await request.json()`

`async for` 구문을 사용하여 요청 본문을 스트림으로 액세스할 수도 있다.

```python
from starlette.requests import Request
from starlette.responses import Response


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    request = Request(scope, receive)
    body = b''
    async for chunk in request.stream():
        body += chunk
    response = Response(body, media_type='text/plain')
    await response(scope, receive, send)
```

`.stream()`을 액세스하면 본문 전체가 메모리에 저장되지 않고 바이트 청크가 제공된다. 이후 `.body()`, `.form()`, `.json()`을 호출하면 오류가 발생한다.

긴 폴링 또는 스트리밍 응답과 같은 일부 경우에는 클라이언트가 연결을 끊었는지 확인해야 할 수 있다. 이 상태는 `disconnected = await request.is_disconnected()`로 확인할 수 있다.

### 요청 파일
요청 파일은 일반적으로 여러 부분으로 구성된 양식 데이터(`multipart/form-data`)로 전송된다.

Signature: `request.form(max_files=1000, max_fields=1000)`

최대 필드 또는 파일 수를 `max_files`와 `max_fields` 매개변수로 설정할 수 있다.

```python
async with request.form(max_files=1000, max_fields=1000):
    ...
```

> **Info**
> 
> 보안상 이유로 이같은 제한을 두고 있다. 필드 또는 파일 수를 무제한으로 허용하면 너무 많은 빈 필드를 구문 분석하는 데 많은 CPU와 메모리가 소모되어 서비스 거부 공격이 발생할 수 있다.

`async with request.form() as form`을 호출하면 파일 업로드와 텍스트 입력을 모두 포함하는 변경할 수 없는 다중 딕셔너리인 `starlette.datastructures.FormData`를 받는다. 파일 업로드 항목은 `starlette.datastructures.UploadFile`의 인스턴스로 표시된다.

`UploadFile`에는 다음과 같은 어트리뷰트가 있다.

- `filename`: 업로드한 원본 파일명이 포함된 `str` 또는 없는 경우 (예: myimage.jpg)`None`이다.
- `content_type`: 콘텐츠 타입(MIME type/meida type)인 `str` 또는 없는 경우 (예: image/jpeg)`None`이다.
- `file`: [SpooledTemporaryFile](https://docs.python.org/3/library/tempfile.html#tempfile.SpooledTemporaryFile)(파일형 객체)입니다. "파일과 유사한" 객체를 기대하는 다른 함수나 라이브러리에 직접 전달할 수 있는 실제 파이썬 파일이다.
- `headers`: `Headers` 객체. `Content-Type` 헤더만 있는 경우가 있지만 다중 파트 필드에 추가 헤더가 포함된 경우 여기에 포함된다. 이 헤더는 `Request.headers`의 헤더와 관계가 없다.
- `size`: 업로드된 파일의 크기(바이트)를 나타내는 `int`이다. 이 값은 요청의 콘텐츠로부터 계산되므로 `Content-Length` 헤더보다 업로드된 파일의 크기를 찾는 데 더 적합하다. 설정하지 않으면 `None`이다.

`UploadFile`에는 다음과 같은 `async` 메서드가 있다. 이 메서드들은 (내부 `SpooledTemporaryFile` 사용하여) 모두 그 아래에 있는 해당 파일 메서드를 호출한다.

- `async write(data)`: 파일에 `data(bytes)`를 쓴다.
- `async read(size)`: 파일의 `size(int)` 바이트를 읽는다.
- `async seek(offset)`: 파일의 바이트 위치 `offset(int)`으로 이동한다.
    - 예를 들어, `await myfile.seek(0)`는 파일의 시작 부분으로 이동한다.
- `async close()`: 파일을 닫는다.

이 모든 메서드는 `async` 메서드이므로 "await"해야 한다.

예를 들어 파일 이름과 내용을 다음과 같이 가져올 수 있다.

```python
async with request.form() as form:
    filename = form["upload_file"].filename
    contents = await form["upload_file"].read()
```

> **Info**
> 
> [RFC-7578: 4.2](https://www.ietf.org/rfc/rfc7578.txt)에 정해진 대로, `Content-Disposition` 헤더에 `name`과 `filename` 필드가 있는 것으로 가정되는 파일을 포함하는 양식 데이터 콘텐츠 부분이다: ``Content-Disposition: form-data; name="user"; filename="somefile"`. `filename` 필드는 RFC-7578에 따라 선택 사항이지만 Starlette에서 어떤 데이터를 파일로 처리해야 하는지 구분하는 데 도움이 된다. `filename` 필드가 제공되면 해당 파일을 액세스하기 위해 `UploadFile` 객체가 생성되고, 그렇지 않으면 양식 데이터 부분이 파싱되어 원시 문자열로 제공된다.

### 어플리케이션
`request.app`을 통해 원래 Starlette 어플리케이션을 액세스할 수 있다.

### 기타 상태
요청에 대한 추가 정보를 저장하려면 `request.state`를 사용하여 저장할 수 있다.

예: 
```python
request.state.time_started = time.time()
```
