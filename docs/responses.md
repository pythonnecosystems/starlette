Starlette에는 `send` 채널에서 적절한 ASGI 메시지를 다시 보내는 것을 처리하는 몇 가지 응답 클래스가 포함되어 있다.

## Response
Signature: `Response(content, status_code=200, headers=None, media_type=None)`

- `content` - 문자열 또는 바이트열이다.
- `status_code` - 정수 HTTP 상태 코드.
- `headers` - 문자열 딕셔너리이다.
- `media_type` - 미디어 타입을 나타내는 문자열(예: "text/html")

Starlette은 Content-Length 헤더를 자동으로 포함한다. 또한 media_type을 기반으로 하고 텍스트 타입에 대한 문자 집합을 추가하는 Content-Type 헤더도 포함한다.

응답을 인스턴스화하면 ASGI 어플리케이션 인스턴스로 호출하여 응답을 보낼 수 있다.

```python
from starlette.responses import Response


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = Response('Hello, world!', media_type='text/plain')
    await response(scope, receive, send)
```

### 쿠키 설정
Starlette는 응답 객체에 쿠키를 설정할 수 있는 `set_cookie` 메서드를 제공한다.

Signature: `Response.set_cookie(key, value, max_age=None, expires=None, path="/", domain=None, secure=False, httponly=False, samesite="lax")`

- `key` - 쿠키의 키가 될 문자열 
- `value` - 쿠키의 값이 될 문자열
- `max_age` - 쿠키의 수명을 초 단위로 정의하는 정수이다. 음수이거나 값이 0이면 쿠키는 즉시 삭제된다. `Optional`
- `expires` - 쿠키가 만료될 때까지의 시간(초)을 정의하는 정수 또는 날짜/시각, `Optional`
- `path` - 쿠키가 적용될 경로의 하위 집합을 지정하는 문자열, `Optional`
- `domain` - 쿠키가 유효한 도메인을 지정하는 문자열, `Optional`
- `secure` - 요청이 SSL 및 HTTPS 프로토콜을 사용하여 이루어진 경우에만 쿠키가 서버로 전송됨을 나타내는 부울 값, `Optional`
- `httponly` - `Document.cookie` 속성, `XMLHttpRequest` 또는 `Request API`를 통해 JavaScript에서 쿠키를 액세스할 수 없음을 나타내는 부울 값, `Optional`
- `samesite` - 쿠키의 동일 사이트 전략을 지정하는 문자열, 유효한 값은 `'lax'`, `'strict'` 및 `'none'`이다. 기본값은 `'lax'`이다. `Optional`

### 쿠키 삭제
반대로 Starlette는 설정된 쿠키를 수동으로 만료하는 `delete_cookie` 메서드도 제공한다.

signature: `Response.delete_cookie(key, path='/', domain=None)`

## HMTLResponse
일부 텍스트 또는 바이트를 받아 HTML 응답을 반환한다.

```python
async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = HTMLResponse('<html><body><h1>Hello, world!</h1></body></html>')
    await response(scope, receive, send)
```

## PlainTextResponse
일부 텍스트 또는 바이트를 받아 일반 텍스트 응답을 반환한다.

```python
from starlette.responses import PlainTextResponse


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = PlainTextResponse('Hello, world!')
    await response(scope, receive, send)
```

## JSONResponse
일부 데이터를 가져와 `application/json`으로 인코딩된 응답을 반환한다.

```python
from starlette.responses import JSONResponse


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = JSONResponse({'hello': 'world'})
    await response(scope, receive, send)
```

### 커스텀 JSON 직렬화
JSON 직렬화를 세밀하게 제어해야 하는 경우 `JSONResponse`를 서브클래싱하고 `render` 메서드를 재정의할 수 있다.

예를 들어 `orjson`같은 서드파티 JSON 라이브러리를 사용하려는 경우이다.

```python
from typing import Any

import orjson
from starlette.responses import JSONResponse


class OrjsonResponse(JSONResponse):
    def render(self, content: Any) -> bytes:
        return orjson.dumps(content)
```

일반적으로 특정 엔드포인트를 미세하게 최적화하거나 비표준 객체 타입을 직렬화해야 하는 경우가 아니라면 기본적으로 `JSONResponse`를 사용하는 것이 좋다.

## RedirectResponse
HTTP 리디렉션을 반환한다. 기본적으로 307 상태 코드를 사용한다.

```python
from starlette.responses import PlainTextResponse, RedirectResponse


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    if scope['path'] != '/':
        response = RedirectResponse(url='/')
    else:
        response = PlainTextResponse('Hello, world!')
    await response(scope, receive, send)
```

## StreamingResponse
async generator 또는 일반 generator/iterator를 받아 응답 본문을 스트리밍한다.

```python
from starlette.responses import StreamingResponse
import asyncio


async def slow_numbers(minimum, maximum):
    yield '<html><body><ul>'
    for number in range(minimum, maximum + 1):
        yield '<li>%d</li>' % number
        await asyncio.sleep(0.5)
    yield '</ul></body></html>'


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    generator = slow_numbers(1, 10)
    response = StreamingResponse(generator, media_type='text/html')
    await response(scope, receive, send)
```

[파일과 유사한](https://docs.python.org/3/glossary.html#term-file-like-object) 객체(예: `open()`으로 생성된 객체)는 일반 iterator라는 점을 명심자. 따라서 스트리밍 응답으로 직접 반환할 수 있다.

## FileResponse
응답으로 파일을 비동기적으로 스트리밍한다.

인스턴스화할 때 다른 응답 타입과 다른 인수 집합을 사용한다.

- `path` - 스트리밍할 파일의 파일 경로
- `headers` - 포함할 커스텀 헤더를 딕셔너리로 지정한다.
- `media_type` - 미디어 유형을 나타내는 문자열, 설정하지 않으면 filename 또는 path가 미디어 타입을 유추하는 데 사용된다.
- `filename` - 설정하면 응답 `Content-Disposition`에 포함된다.
- `content_disposition_type` - 응답 `Content-Disposition`에 포함된다. "attachement"(기본값) 또는 "inline"으로 설정할 수 있다.

파일 응답에는 적절한 `Content-Length`, `Last-Modified` 및 `ETag` 헤더를 포함한다.

```python
from starlette.responses import FileResponse


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = FileResponse('statics/favicon.ico')
    await response(scope, receive, send)
```

## Third party response

### [EventSourceResponse](https://github.com/sysid/sse-starlette)
[Server-Sent Events](https://html.spec.whatwg.org/multipage/server-sent-events.html)를 구현하는 응답 클래스이다. 복잡한 웹소켓 없이 서버에서 클라이언트로 이벤트를 스트리밍할 수 있다.

### [baize.asgi.FileResponse](https://baize.aber.sh/asgi#fileresponse)
Starlette `FileResponse`를 원활하게 대체하기 위해 [Head 메서드](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD)와 [Range requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests)을 자동으로 처리한다.
