## <a name="route">HTTP 라우팅</a>
Starlette에는 간단하지만 유능한 요청 라우팅 시스템이 있다. 라우팅 테이블은 경로 목록으로 정의되며 어플리케이션을 인스턴스화할 때 전달된다.

```python
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.routing import Route


async def homepage(request):
    return PlainTextResponse("Homepage")

async def about(request):
    return PlainTextResponse("About")


routes = [
    Route("/", endpoint=homepage),
    Route("/about", endpoint=about),
]

app = Starlette(routes=routes)
```

`endpoint` 인수는 다음 중 하나일 수 있다.

- 단일 요청 인수를 받아들이고 응답을 반환해야 하는 일반 함수 또는 비동기 함수.
- Starlette의 `HTTPEndpoint`와 같이 ASGI 인터페이스를 구현하는 클래스.

## Path 매개변수
경로는 URI 템플릿 스타일을 사용하여 경로 구성 요소를 캡처할 수 있다.

```python
    Route('/users/{username}', user)
```

기본적으로 경로의 끝 또는 다음 `/`까지 문자를 캡처한다.
변환기를 사용하여 캡처되는 내용을 수정할 수 있다. 사용 가능한 변환기는 다음과 같다.

- `str`은 문자열을 반환하며 기본값이다.
- `int`는 Python 정수를 반환한다.
- `float`는 Python 실수를 반환한다.
- `uuid`는 Python `uuid.UUID` 인스턴스를 반환한다.
- `path`는 추가 `/` 문자를 포함한 나머지 경로를 반환한다.

변환기는 다음과 같이 콜론으로 접두사를 붙여 사용한다.

```python
Route('/users/{user_id:int}', user)
Route('/floating-point/{number:float}', floating_point)
Route('/uploaded/{rest_of_path:path}', uploaded)
```

정의되지 않은 다른 변환기가 필요한 경우 직접 만들 수도 있다. 아래에서 `datetime` 변환기를 만드는 방법과 등록하는 방법에 대한 예를 참조하자.

```python
from datetime import datetime

from starlette.convertors import Convertor, register_url_convertor


class DateTimeConvertor(Convertor):
    regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(.[0-9]+)?"

    def convert(self, value: str) -> datetime:
        return datetime.strptime(value, "%Y-%m-%dT%H:%M:%S")

    def to_string(self, value: datetime) -> str:
        return value.strftime("%Y-%m-%dT%H:%M:%S")

register_url_convertor("datetime", DateTimeConvertor())
```

등록한 후에는 다음과 같이 사용할 수 있다.

```python
    Route('/history/{date:datetime}', history)
```

경로 매개변수는 요청에서 `request.path_params` 딕셔너리로 사용할 수 있다.

```python
async def user(request):
    user_id = request.path_params['user_id']
    ...
```

## HTTP 메소드 처리
라우트는 엔드포인트에서 처리할 HTTP 메서드를 지정할 수도 있다.

```python
    Route('/users/{user_id:int}', user, methods=["GET", "POST"])
```

기본적으로 함수 엔드포인트를 지정하지 않으면 `GET` 요청만 수락한다.

## 라우트 서브마운팅
대규모 어플리케이션의 경우 공통 경로 접두사를 기반으로 라우팅 테이블의 일부를 분리할 수 있다.

```python
routes = [
    Route('/', homepage),
    Mount('/users', routes=[
        Route('/', users, methods=['GET', 'POST']),
        Route('/{username}', user),
    ])
]
```

이 스타일을 사용하면 프로젝트의 여러 부분에서 라우팅 테이블의 다양한 하위 집합을 정의할 수 있다.

```python
from myproject import users, auth

routes = [
    Route('/', homepage),
    Mount('/users', routes=users.routes),
    Mount('/auth', routes=auth.routes),
]
```

마운팅을 사용하여 Starlette 어플리케이션 내에 하위 어플리케이션을 포함할 수도 있다. 예를 들어...

```python
# This is a standalone static files server:
app = StaticFiles(directory="static")

# This is a static files server mounted within a Starlette application,
# underneath the "/static" path.
routes = [
    ...
    Mount("/static", app=StaticFiles(directory="static"), name="static")
]

app = Starlette(routes=routes)
```

## 역방향 URL 조회
리디렉션 응답을 반환해야 하는 경우와 같이 특정 경로에 대한 URL을 생성할 수 있어야 하는 경우가 종종 있다.

- Signature: `url_for(name, **path_params) -> URL`

```python
routes = [
    Route("/", homepage, name="homepage")
]

# We can use the following to return a URL...
url = request.url_for("homepage")
```

URL 조회에는 경로 매개변수가 포함될 수 있다...

```python
routes = [
    Route("/users/{username}", user, name="user_detail")
]

# We can use the following to return a URL...
url = request.url_for("user_detail", username=...)
```

`Mount`에 `name`이 포함된 경우 서브마운트는 역방향 URL 조회를 위해 `{prefix}:{name}` 스타일을 사용해야 한다.

```python
routes = [
    Mount("/users", name="users", routes=[
        Route("/", user, name="user_list"),
        Route("/{username}", user, name="user_detail")
    ])
]

# We can use the following to return URLs...
url = request.url_for("users:user_list")
url = request.url_for("users:user_detail", username=...)
```

마운트된 어플리케이션에는 `path=...`` 매개변수가 포함될 수 있다.

```python
routes = [
    ...
    Mount("/static", app=StaticFiles(directory="static"), name="static")
]

# We can use the following to return URLs...
url = request.url_for("static", path="/css/base.css")
```

`request` 인스턴스가 없는 경우 어플리케이션에 대해 역방향 조회를 수행할 수 있지만, 이 경우 URL 경로만 반환된다.

```python
    url = app.url_path_for("user_detail", username=...)
```

## 호스트 기반 라우팅
`Host` 헤더를 기준으로 동일한 경로에 대해 다른 경로를 사용하려는 경우.

일치 시 `Host` 헤더에서 포트가 제거된다는 점에 유의하자. 예를 들어 Host 헤더에 `3600` 이외의 포트가 포함되어 있거나 포함되어 있지 않은 경우(예: `example.org:5600, example.org`)에도 `Host(host='example.org:3600', ...)`가 처리된다. 따라서 `url_for`에서 사용할 포트가 필요한 경우 포트를 지정할 수 있다.

호스트 기반 경로를 어플리케이션에 연결하는 방법에는 여러 가지가 있다.

```python
site = Router()  # Use eg. `@site.route()` to configure this.
api = Router()  # Use eg. `@api.route()` to configure this.
news = Router()  # Use eg. `@news.route()` to configure this.

routes = [
    Host('api.example.org', api, name="site_api")
]

app = Starlette(routes=routes)

app.host('www.example.org', site, name="main_site")

news_host = Host('news.example.org', news)
app.router.routes.append(news_host)
```

URL 조회에는 경로 매개변수와 마찬가지로 호스트 매개변수가 포함될 수 있다.

```python
routes = [
    Host("{subdomain}.example.org", name="sub", app=Router(routes=[
        Mount("/users", name="users", routes=[
            Route("/", user, name="user_list"),
            Route("/{username}", user, name="user_detail")
        ])
    ]))
]
...
url = request.url_for("sub:users:user_detail", username=..., subdomain=...)
url = request.url_for("sub:users:user_list", subdomain=...)
```

## 라우트 우선순위
들어오는 경로는 각 `Route`와 순서대로 일치합니다.

들어오는 경로에 하나 이상의 경로가 일치할 수 있는 경우에는 일반적인 경우보다 구체적인 경로가 먼저 나열되도록 주의해야 한다.

예를 들어,

```python
# Don't do this: `/users/me` will never match incoming requests.
routes = [
    Route('/users/{username}', user),
    Route('/users/me', current_user),
]

# Do this: `/users/me` is tested first.
routes = [
    Route('/users/me', current_user),
    Route('/users/{username}', user),
]
```

## 라우터 인스턴스로 작업
낮은 수준에서 작업하는 경우 `Starlette` 어플리케이션을 만드는 대신 일반 `Router` 인스턴스를 사용하는 것이 좋다. 이렇게 하면 미들웨어로 래핑하지 않고 어플리케이션 라우팅만 제공하는 경량 ASGI 애플리케이션을 사용할 수 있다.

```python
app = Router(routes=[
    Route('/', homepage),
    Mount('/users', routes=[
        Route('/', users, methods=['GET', 'POST']),
        Route('/{username}', user),
    ])
])
```

## WebSocket 라우팅
WebSocket 엔드포인트로 작업할 때는 일반적인 `Route` 대신 `WebSocketRoute`를 사용해야 한다.

경로 매개변수 및 `WebSocketRoute`의 역방향 URL 조회는 위의 HTTP [Route](#route) 섹션에서 확인할 수 있는 HTTP `Route`와 동일하게 작동한다.

```python
from starlette.applications import Starlette
from starlette.routing import WebSocketRoute


async def websocket_index(websocket):
    await websocket.accept()
    await websocket.send_text("Hello, websocket!")
    await websocket.close()


async def websocket_user(websocket):
    name = websocket.path_params["name"]
    await websocket.accept()
    await websocket.send_text(f"Hello, {name}")
    await websocket.close()


routes = [
    WebSocketRoute("/", endpoint=websocket_index),
    WebSocketRoute("/{name}", endpoint=websocket_user),
]

app = Starlette(routes=routes)
```

`endpoint` 인수는 다음 중 하나일 수 있다.

- 단일 `websocket` 인수를 받는 비동기 함수.
- Starlette의 `WebSocketEndpoint`와 같이 ASGI 인터페이스를 구현하는 클래스.
