Starlette에는 지정된 디렉터리에 있는 파일을 제공하기 위한 `StaticFiles` 클래스도 포함되어 있다.

## StaticFiles

Signature: `StaticFiles(directory=None, packages=None, html=False, check_dir=True, follow_symlink=False)`

- `directory` - 디렉토리 경로를 나타내는 문자열 또는 [os.Pathlike](https://docs.python.org/3/library/os.html#os.PathLike).
- `packages` - python 패키지의 문자열 리스트 또는 문자열 튜플 리스트이다.
- `html`` - HTML 모드로 실행한다. 디렉터리에 대한 `index.html` 파일이 있으면 자동으로 로드한다.
- `check_dir` - 인스턴스화할 때 디렉터리가 존재하는지 확인gks다. 기본값은 `True`이다.
- `follow_symlink` - 파일과 디렉터리에 대한 심볼릭 링크를 따라야 하는지 여부를 나타내는 부울이다. 기본값은 `False`이다.

이 ASGI 어플리케이션을 Starlette의 라우팅과 결합하여 포괄적인 정적 파일 서비스를 제공할 수 있다.

```python
from starlette.applications import Starlette
from starlette.routing import Mount
from starlette.staticfiles import StaticFiles


routes = [
    ...
    Mount('/static', app=StaticFiles(directory='static'), name="static"),
]

app = Starlette(routes=routes)
```

정적 파일은 일치하지 않는 요청에 대해 "404 찾을 수 없음" 또는 "405 메서드 허용되지 않음" 응답을 반환한다. HTML 모드에서 `404.html` 파일이 존재하면 404 응답으로 표시된다.

`packages` 옵션은 python 패키지에 포함된 "정적" 디렉터리를 포함하는 데 사용할 수 있다. Python "bootstrap4" 패키지가 이에 대한 예이다.

```python
from starlette.applications import Starlette
from starlette.routing import Mount
from starlette.staticfiles import StaticFiles


routes=[
    ...
    Mount('/static', app=StaticFiles(directory='static', packages=['bootstrap4']), name="static"),
]

app = Starlette(routes=routes)
```

기본적으로 `StaticFiles`는 각 패키지에서 `statics` 디렉터리를 찾지만 문자열 튜플을 지정하여 기본 디렉터리를 변경할 수 있다.

```python
    routes=[
    ...
    Mount('/static', app=StaticFiles(packages=[('bootstrap4', 'static')]), name="static"),
]
```

Python 패키징을 사용하여 정적 파일을 포함하는 대신 "static" 디렉토리에 직접 정적 파일을 포함하는 것을 선호할 수도 있지만, 재사용 가능한 컴포넌트를 번들로 묶는 데 유용할 수 있다.
