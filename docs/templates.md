스타렛은 특정 템플릿 엔진과 엄격하게 결합되어 있지는 않지만, Jinja2는 탁월한 선택이다.

## Jinja2 템플릿
Signature: `Jinja2Templates(directory, context_processors=None, **env_options)`

- `directory` - 디렉토리 경로를 나타내는 문자열, [os.Pathlike](https://docs.python.org/3/library/os.html#os.PathLike) 또는 문자열 또는 [os.Pathlike](https://docs.python.org/3/library/os.html#os.PathLike)의 리스트.
- `context_processors` - 템플릿 컨텍스트에 추가할 사전을 반환하는 함수 리스트이다.
- `**env_options` - Jinja2 환경에 전달할 추가 키워드 인자.

Starlette는 jinja2를 구성하는 간단한 방법을 제공한다. 이 방법을 기본으로 사용하실 수 있다.

```python
from starlette.applications import Starlette
from starlette.routing import Route, Mount
from starlette.templating import Jinja2Templates
from starlette.staticfiles import StaticFiles


templates = Jinja2Templates(directory='templates')

async def homepage(request):
    return templates.TemplateResponse(request, 'index.html')

routes = [
    Route('/', endpoint=homepage),
    Mount('/static', StaticFiles(directory='static'), name='static')
]

app = Starlette(debug=True, routes=routes)
```

들어오는 요청 인스턴스는 템플릿 컨텍스트의 일부로 포함되어야 한다는 점에 유의하자.

Jinja2 템플릿 컨텍스트에는 자동으로 `url_for` 함수가 포함되므로 어플리케이션 내의 다른 페이지로 올바르게 하이퍼링크할 수 있다.

예를 들어 HTML 템플릿 내에서 정적 파일에 링크할 수 있다.

```html
<link href="{{ url_for('static', path='/css/bootstrap.min.css') }}"
``` 

[custom filters](https://jinja.palletsprojects.com/en/3.0.x/api/?highlight=environment#writing-filters)를 사용하려면 Jinja2Template의 `env` 속성을 업데이트해야 합니다.

```python
from commonmark import commonmark
from starlette.templating import Jinja2Templates

def marked_filter(text):
    return commonmark(text)

templates = Jinja2Templates(directory='templates')
templates.env.filters['marked'] = marked_filter
```

## 커스텀 jinja2.Environment 인스턴스 사용
Starlette는 사전 구성된 `jinja2.Environment` 인스턴스도 허용한다.

```python
import jinja2
from starlette.templating import Jinja2Templates

env = jinja2.Environment(...)
templates = Jinja2Templates(env=env)
```

## 컨텍스트 프로세서
컨텍스트 프로세서는 템플릿 컨텍스트에 병합할 딕셔너리을 반환하는 함수이다. 모든 함수는 하나의 인수 `request`만 받으며 컨텍스트에 추가할 사전을 반환해야 한다.

템플릿 프로세서의 일반적인 사용 사례는 공유 변수를 사용하여 템플릿 컨텍스트를 확장하는 것이다.

```python
import typing
from starlette.requests import Request

def app_context(request: Request) -> typing.Dict[str, typing.Any]:
    return {'app': request.app}
```

### 컨텍스트 탬플릿 등록
컨텍스트 프로세서를 Jinja2Template 클래스의 `context_processors` 인수에 전달한다.

```python
import typing

from starlette.requests import Request
from starlette.templating import Jinja2Templates

def app_context(request: Request) -> typing.Dict[str, typing.Any]:
    return {'app': request.app}

templates = Jinja2Templates(
    directory='templates', context_processors=[app_context]
)
```

> **Info**
>
> 컨텍스트 프로세서를 비동기 함수로 지원하지 않는다.

## 템플릿 응답 테스트
테스트 클라이언트를 사용할 때 템플릿 응답에는 `.template`와 `.context` 속성이 포함된다.

```python
from starlette.testclient import TestClient


def test_homepage():
    client = TestClient(app)
    response = client.get("/")
    assert response.status_code == 200
    assert response.template.name == 'index.html'
    assert "request" in response.context
```

## Jinja2.Environment 커스터마이징
`Jinja2Templates`는 진자2 `Environment`가 지원하는 모든 옵션을 허용한다. 이렇게 하면 Starlette이 생성한 `Environment` 인스턴스를 더 많이 제어할 수 있다.

`Environment`에서 사용할 수 있는 옵션 목록은 [여기](https://jinja.palletsprojects.com/en/3.0.x/api/#jinja2.Environment) Jinja2 설명서에서 확인할 수 있다.

```python
from starlette.templating import Jinja2Templates


templates = Jinja2Templates(directory='templates', autoescape=False, auto_reload=True)
```

## 비동기 테플릿 렌더링
Jinja2는 비동기 템플릿 렌더링을 지원하지만, 일반적으로 템플릿에 데이터베이스 조회 또는 기타 I/O 작업을 호출하는 로직이 없도록 하는 것이 좋다.

대신 엔드포인트가 모든 I/O를 수행하도록 하는 것이 좋다. 예를 들어 뷰 내에서 모든 데이터베이스 쿼리를 엄격하게 평가하고 최종 결과는 컨텍스트에 포함시키는 것이 좋다.
