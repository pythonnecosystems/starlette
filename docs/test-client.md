테스트 클라이언트를 사용하면 `httpx` 라이브러리를 사용하여 ASGI 어플리케이션에 대한 요청을 할 수 있다.

```python
from starlette.responses import HTMLResponse
from starlette.testclient import TestClient


async def app(scope, receive, send):
    assert scope['type'] == 'http'
    response = HTMLResponse('<html><body>Hello, world!</body></html>')
    await response(scope, receive, send)


def test_app():
    client = TestClient(app)
    response = client.get('/')
    assert response.status_code == 200
```

테스트 클라이언트는 다른 `httpx` 세션과 동일한 인터페이스를 노출한다. 특히 요청을 위한 호출은 어웨이터블(awaitables)이 아닌 표준 함수 호출이라는 점에 유의하자.

인증, 세션 쿠키 처리 또는 파일 업로드와 같은 모든 `httpx` 표준 API를 사용할 수 있다.

예를 들어 테스트 클라이언트에서 헤더를 설정하려면 다음을 수행할 수 있다.

```python
client = TestClient(app)

# Set headers on the client for future requests
client.headers = {"Authorization": "..."}
response = client.get("/")

# Set headers for each request separately
response = client.get("/", headers={"Authorization": "..."})
```

예를 들어 TestClient로 파일을 전송할 수 있다.

```python
client = TestClient(app)

# Send a single file
with open("example.txt", "rb") as f:
    response = client.post("/form", files={"file": f})

# Send multiple files
with open("example.txt", "rb") as f1:
    with open("example.png", "rb") as f2:
        files = {"file1": f1, "file2": ("filename", f2, "image/png")}
        response = client.post("/form", files=files)
```

자세한 내용은 [`httpx` 문서](https://www.python-httpx.org/advanced/)에서 확인할 수 있다.

기본적으로 `TestClient`는 어플리케이션에서 발생하는 모든 예외를 발생시킨다. 때로는 클라이언트가 서버 예외를 발생시키지 않고 500 오류 응답의 내용을 테스트하고 싶을 수 있다. 이 경우 `client = TestClient(app, raise_server_exceptions=False)`를 사용해야 한다.

> **Note**
>
> `TestClient`가 `lifespan` 핸들러를 실행하도록 하려면 `TestClient`를 컨텍스트 관리자로 사용해야 한다. `TestClient`가 인스턴스화될 때는 트리거되지 않는다. 자세한 내용은 [여기](./lifespan.md#running-lifespan-in-tests)에서 확인할 수 있다.

## Async 백엔드 선택
`TestClient`는 인자로 `backend`(문자열)와 `backend_options`(딕셔너리)를 받는다. 이 옵션은 `anyio.start_blocking_portal()`에 전달된다. 허용되는 백엔드 옵션에 대한 자세한 내용은 [anyio 문서](https://anyio.readthedocs.io/en/stable/basics.html#backend-options)를 참조한다. 기본적으로 기본 옵션으로 `asyncio`이 사용된다.

`Trio`를 실행하려면 `backend="trio"`를 전달한다. 예를 들면,

```python
def test_app()
    with TestClient(app, backend="trio") as client:
       ...
```

`uvloop`로 `asyncio`를 실행하려면, `backend_options={"use_uvloop": True}`를 전달한다. 예를 들면,

```python
def test_app()
    with TestClient(app, backend_options={"use_uvloop": True}) as client:
       ...
```

## WebSocket 세션 테스팅

테스트 클라이언트로 websocket 세션을 테스트할 수도 있다.

`httpx` 라이브러리는 초기 핸드셰이크를 빌드하는 데 사용되므로 http와 websocket 테스트 모두에서 동일한 인증 옵션과 기타 헤더를 사용할 수 있다.

```python
from starlette.testclient import TestClient
from starlette.websockets import WebSocket


async def app(scope, receive, send):
    assert scope['type'] == 'websocket'
    websocket = WebSocket(scope, receive=receive, send=send)
    await websocket.accept()
    await websocket.send_text('Hello, world!')
    await websocket.close()


def test_app():
    client = TestClient(app)
    with client.websocket_connect('/') as websocket:
        data = websocket.receive_text()
        assert data == 'Hello, world!'
```

세션에 대한 작업은 어웨이터블이 아닌 표준 함수 호출이다.

컨텍스트 관리형 `with` 블록 내에서 세션을 사용하는 것이 중요하다. 이렇게 하면 ASGI 어플리케이션이 실행되는 백그라운드 스레드가 올바르게 종료되고 어플리케이션 내에서 발생하는 모든 예외가 항상 테스트 클라이언트에 의해 발생하도록 보장할 수 있다.

### 테스트 세션 수립

- `.websocket_connect(url, subprotocol=None, **options)` - `httpx.get()`과 동일한 인수 집합을 받는다.

어플리케이션이 websocket 연결을 수락하지 않는 경우` starlette.websockets.WebSocketDisconnect`가 발생할 수 있다.

`websocket_connect()`를 (`with` 블록 내에서) 컨텍스트 관리자로 사용해야 한다.

> **Note**
>
> `websocket_connect`에서 `params` 인수를 지원하지 않는다. 쿼리 인수를 전달해야 하는 경우 URL에 직접 하드코딩하여야 한다
>
> ```
> with client.websocket_connect('/path?foo=bar') as websocket:
>     ...
> ```

### 데이터 보내기
- `.send_text(data)` - 지정된 텍스트를 어플리케이션으로 보낸다.
- `.send_bytes(data)` - 지정된 바이트를 어플리케이션으로 보낸다.
- `.send_json(data, mode="text")` - 지정된 데이터를 어플리케이션으로 보낸다. 바이너리 데이터 프레임을 통해 JSON을 보내려면 `mode="binary"`를 사용한다

### 데이터 받기
- `.receive_text()` - 어플리케이션이 보낸 텍스트를 기다렸다가 반환한다.
- `.receive_bytes()` - 어플리케이션이 보낸 바이트 문자열을 기다렸다가 반환한다.
- `.receive_json(mode="text")` - 어플리케이션이 보낸 json 데이터를 기다렸다가 반환한다. 바이너리 데이터 프레임을 통해 JSON을 수신하려면 `mode="binary"`를 사용한다.

`starlette.websockets.WebSocketDisconnect`를 발생시킬 수 있다.

### 연결 닫기
- `.close(code=1000)` - websocket 연결의 클라이언트 측 종료를 수행한다.

## 비동기 테스트
때로는 어플리케이션 외부에서 비동기 작업을 수행하고 싶을 때가 있다. 예를 들어, 기존 비동기 데이터베이스 클라이언트/인프라를 사용하여 앱을 호출한 후 데이터베이스의 상태를 확인하고 싶을 수 있다.

이러한 상황에서는 자체 이벤트 루프를 생성하고 데이터베이스 연결과 같은 비동기 리소스를 이벤트 루프 간에 공유할 수 없는 경우가 많기 때문에 `TestClient`를 사용하기가 어렵다. 이 문제를 해결하는 가장 간단한 방법은 전체 테스트를 비동기화하여 [httpx.AsyncClient](https://www.python-httpx.org/advanced/#calling-into-python-web-apps)와 같은 비동기 클라이언트를 사용하는 것이다.

다음은 이러한 테스트의 예이다.

```python
from httpx import AsyncClient
from starlette.applications import Starlette
from starlette.routing import Route
from starlette.requests import Request
from starlette.responses import PlainTextResponse


def hello(request: Request) -> PlainTextResponse:
    return PlainTextResponse("Hello World!")


app = Starlette(routes=[Route("/", hello)])


# if you're using pytest, you'll need to to add an async marker like:
# @pytest.mark.anyio  # using https://github.com/agronholm/anyio
# or install and configure pytest-asyncio (https://github.com/pytest-dev/pytest-asyncio)
async def test_app() -> None:
    # note: you _must_ set `base_url` for relative urls like "/" to work
    async with AsyncClient(app=app, base_url="http://testserver") as client:
        r = await client.get("/")
        assert r.status_code == 200
        assert r.text == "Hello World!"
```