Starlette에는 HTTP 요청과 유사한 역할을 수행하지만 websocket에서 데이터를 송수신할 수 있는 `WebSocket` 클래스가 포함되어 있다.

## WebSocket

Signature: `WebSocket(scope, receive=None, send=None)`

```python
from starlette.websockets import WebSocket


async def app(scope, receive, send):
    websocket = WebSocket(scope=scope, receive=receive, send=send)
    await websocket.accept()
    await websocket.send_text('Hello, world!')
    await websocket.close()
```

WebSocket은 매핑 인터페이스를 제공하므로 스`scope`와 같은 방식으로 사용할 수 있다.

예: websocket['path']는 ASGI 경로를 반환한다.

### URL
websocket URL을 `websocket.url`로 액세스할 수 있다.

이 속성은 실제로 `str`의 서브클래스이며 URL에서 파싱할 수 있는 모든 컴포넌트를 노출한다.

예: `websocket.url.path`, `websocket.url.port`, `websocket.url.scheme`

### Headers
헤더는 대소문자를 구분하지 않는 변경되지 않는 다중 딕셔너리로 노출된다.

예: `websocket.headers['sec-websocket-version']`

### Query 매개변수
쿼리 매개변수는 변경되지 않는 다중 딕셔너리로 노출된다.

예: `websocket.query_params['search']`

### Path 매개변수
라우터 경로 매개변수는 딕셔너리 인터페이스로 노출된다.

예: `websocket.path_params['username']`

## 연결 수락 (Accepting the connection)
- `await websocket.accept(subprotocol=None, headers=None)`

## 데이터 송신
- `await websocket.send_text(data)`
- `await websocket.send_bytes(data)`
- `await websocket.send_json(data)`

JSON 메시지는 버전 0.10.0 이상부터 기본적으로 텍스트 데이터 프레임을 통해 전송된다. 바이너리 데이터 프레임을 통해 JSON을 전송하려면 `websocket.send_json(data, mode="binary")`을 사용하여야 한다.

## 데이터 수신
- `await websocket.receive_text()`
- `await websocket.receive_bytes()`
- `await websocket.receive_json()`

`starlette.websockets.WebSocketDisconnect()`를 발생시킬 수 있다.

버전 0.10.0 이상부터 JSON 메시지는 기본적으로 텍스트 데이터 프레임을 통해 수신된다. 바이너리 데이터 프레임을 통해 JSON을 수신하려면 `websocket.receive_json(data, mode="binary")`을 사용하여야 한다.

## 데이터 반복
- `websocket.iter_text()`
- `websocket.iter_bytes()`
- `websocket.iter_json()`

`receive_text`, `receive_bytes` 및 `receive_json`과 유사하지만 비동기 이터러를 반환한다.

```python
from starlette.websockets import WebSocket


async def app(scope, receive, send):
    websocket = WebSocket(scope=scope, receive=receive, send=send)
    await websocket.accept()
    async for message in websocket.iter_text():
        await websocket.send_text(f"Message text was: {message}")
    await websocket.close()
```

`starlette.websockets.WebSocketDisconnect`가 발생하면 이터레이터가 종료된다.

## 연결 종료
- `await websocket.close(code=1000, reason=None)`

## 메시지 송수신
미가공 ASGI 메시지를 보내거나 받아야 하는 경우 미가동 `send` 및 `receive` 콜러블을 사용하는 대신 `websocket.send()` 및 `websocket.receive()`를 사용해야 한다. 이렇게 하면 웹소켓의 상태를 올바르게 업데이트할 수 있다.

- `await websocket.send(message)`
- `await websocket.receive()`
